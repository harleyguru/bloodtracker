BEGIN TRANSACTION;

CREATE TABLE `test_group1` (
	`_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`test_id`	INTEGER,
	`group_id`	INTEGER,
	FOREIGN KEY(`group_id`) REFERENCES `groups`(`_id`)
);

insert into test_group1 select * from test_group;

drop table test_group;

CREATE TABLE `test_unit1` (
	`_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`test_id`	INTEGER,
	`unit_id`	INTEGER,
	`activate`	INTEGER,
	FOREIGN KEY(`unit_id`) REFERENCES `units`(`_id`)
);

insert into test_unit1 select * from test_unit;

drop table test_unit;

CREATE TABLE `tests1` (
	`_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	TEXT NOT NULL,
	`abbreviation`	TEXT,
	`info`	TEXT,
	`min`	REAL NOT NULL,
	`max`	REAL NOT NULL,
	`duration`	INTEGER,
	`starttime`	REAL,
	`endtime`	REAL
);

insert into tests1 select * from tests;

drop table tests;

CREATE TABLE `tests` (
	`_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	TEXT NOT NULL,
	`abbreviation`	TEXT,
	`info`	TEXT,
	`min`	REAL NOT NULL,
	`max`	REAL NOT NULL,
	`duration`	INTEGER DEFAULT 6,
	`starttime`	REAL,
	`endtime`	REAL
);

insert into tests select * from tests1;

drop table tests1;

CREATE TABLE `test_group` (
	`_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`test_id`	INTEGER,
	`group_id`	INTEGER,
	FOREIGN KEY(`test_id`) REFERENCES `tests`(`_id`),
	FOREIGN KEY(`group_id`) REFERENCES `groups`(`_id`)
);

insert into test_group select * from test_group1;

drop table test_group1;

CREATE TABLE `test_unit` (
	`_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`test_id`	INTEGER,
	`unit_id`	INTEGER,
	`activate`	INTEGER,
	FOREIGN KEY(`test_id`) REFERENCES `tests`(`_id`),
	FOREIGN KEY(`unit_id`) REFERENCES `units`(`_id`)
);

insert into test_unit select * from test_unit1;

drop table test_unit1;

COMMIT;