//
//  ColorBar.swift
//  myBloodTracker
//
//  Created by harleyguru on 9/21/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit
import SwiftCharts

@IBDesignable

class ColorBar: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    var min: Double! = 50
    var max: Double! = 100
    @IBInspectable var startColor: UIColor = UIColor(red:0.99, green:0.50, blue:0.50, alpha:1.0)
    @IBInspectable var endColor: UIColor = UIColor(red:0.30, green:0.77, blue:0.48, alpha:1.0)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.isOpaque = true
        self.backgroundColor = .clear
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        
        // fill red background in round rect
        var path = UIBezierPath(roundedRect: rect,
                                byRoundingCorners: UIRectCorner.allCorners,
                                cornerRadii: CGSize(width: 2.0, height: 2.0))
        path.addClip()
        startColor.setFill()
        path.fill()
        
        // fill green region over red rect
        let greenRect = CGRect(x: rect.origin.x, y: rect.size.height - CGFloat(max!), width: rect.size.width, height: CGFloat(max! - min!))
        path = UIBezierPath(rect: greenRect)
        endColor.setFill()
        path.fill()
        
        // apply gradient effect
        let context = UIGraphicsGetCurrentContext()
        var colors = [startColor.cgColor, endColor.cgColor]
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let colorLocations:[CGFloat] = [0.0, 1.0]
        var gradient = CGGradient(colorsSpace: colorSpace, colors: colors as CFArray, locations: colorLocations)
        var startPoint = CGPoint(x: rect.origin.x, y: rect.size.height - CGFloat(max!) - 10)
        var endPoint = CGPoint(x:rect.origin.x, y: rect.size.height - CGFloat(max!) + 10)
        context?.drawLinearGradient(gradient!, start: startPoint, end: endPoint, options: CGGradientDrawingOptions(rawValue: UInt32(0)))
        
        colors = [endColor.cgColor, startColor.cgColor]
        gradient = CGGradient(colorsSpace: colorSpace, colors: colors as CFArray, locations: colorLocations)
        startPoint = CGPoint(x: rect.origin.x, y: rect.size.height - CGFloat(min!) - 10)
        endPoint = CGPoint(x:rect.origin.x, y: rect.size.height - CGFloat(min!) + 10)
        context?.drawLinearGradient(gradient!, start: startPoint, end: endPoint, options: CGGradientDrawingOptions(rawValue: UInt32(0)))
        
    }
}
