//
//  InfoCell.swift
//  myBloodTracker
//
//  Created by harleyguru on 8/3/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit

class InfoCell: UITableViewCell {
    
    /// reuse identifier for this cell
    static var identifier: String {
        get {
            return String.className(aClass: self)
        }
    }
    
    /// text view represents information of test
    @IBOutlet weak var infoTextView: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
