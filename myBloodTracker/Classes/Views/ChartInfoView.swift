//
//  ChartInfoView.swift
//  myBloodTracker
//
//  Created by harleyguru on 10/13/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit

/// View to show detail information of chart point
class ChartInfoView: UIView {
     
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.isOpaque = true
        self.backgroundColor = .white
        
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.borderWidth = 2
        self.layer.cornerRadius = frame.height / 2
    }
    
    /// convenience initializer for border color
    ///
    /// - parameter frame:       frame of this view
    /// - parameter borderColor: border color to use for this view
    ///
    /// - returns: ChartInfoView object
    convenience init(frame: CGRect, borderColor: UIColor) {
        self.init(frame: frame)
        
        self.layer.borderColor = borderColor.cgColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
}
