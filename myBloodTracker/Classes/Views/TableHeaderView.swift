//
//  TableHeaderView.swift
//  myBloodTracker
//
//  Created by harleyguru on 8/2/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit

class TableHeaderView: UITableViewHeaderFooterView {
    
    /// title label for this header view
    @IBOutlet weak var titleLabel: UILabel!
    
    /// reuse identifier for this header/footer view
    static var identifier: String {
        get {
            return String.className(aClass: self)
        }
    }

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
