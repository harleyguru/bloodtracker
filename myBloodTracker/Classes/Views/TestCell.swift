//
//  TestCell.swift
//  myBloodTracker
//
//  Created by harleyguru on 6/26/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit

/// Represents table view cell for Select Lab Test table
class TestCell: UITableViewCell {
    
    /// test title label
    @IBOutlet weak var titleLabel: UILabel!
    
    /// test abbreviation label
    @IBOutlet weak var abbreviationLabel: UILabel!
    
    /// last test value and unit or unit label when using in settings table
    @IBOutlet weak var valueLabel: UILabel!
    
    /// min - max limit label, this will be valid only when this cell will be used in settings page
    @IBOutlet weak var limitLabel: UILabel!
    
    static var identifier: String {
        get {
            return String.className(aClass: self)
        }
    }
    
    /// test for this cell to represent
    var _test: Test!
    
    /// Description
    var test: Test! {
        get {
            return _test
        }
        
        set {
            _test = newValue
            self.titleLabel.text = _test.name
            self.abbreviationLabel.text = _test.abbreviation
            
            if self.limitLabel == nil { // this is cell for TestVC
                self.valueLabel.text = "\(_test.value)"
                
                if let lastValue = _test.value {
                    
                    var attribute = [NSForegroundColorAttributeName: UIColor(red:0.26, green:0.61, blue:0.26, alpha:1.0)]
                    
                    if lastValue < _test.min! || lastValue > _test.max! {
                        attribute = [NSForegroundColorAttributeName: UIColor.red]
                    }
                    
                    let attributedValueString = NSMutableAttributedString(string: "\(lastValue)", attributes: attribute)
                    attributedValueString.append(NSAttributedString(string: " " + _test.activeUnit!.name!))
                    
                    self.valueLabel.attributedText = attributedValueString
                    
                } else {
                    self.valueLabel.text = nil
                }
            } else { // this is cell for SettingsVC
                
                self.valueLabel.text = "\((_test.activeUnit!.name)!)"
                
                var limit = ""
                if self.test.min == 0 {
                    limit = "< \((self.test.max)!)"
                } else {
                    limit = "\((self.test.min)!) - \((self.test.max)!)"
                }
                
                self.limitLabel.text = limit
            }
            
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
