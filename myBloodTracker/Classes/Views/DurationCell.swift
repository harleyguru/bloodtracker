//
//  DurationCell.swift
//  myBloodTracker
//
//  Created by harleyguru on 10/17/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit

/// Cell for start and end date setting in duration setting screen
class DurationCell: UITableViewCell {
    
    /// start date label
    @IBOutlet weak var startDateButton: UIButton!
    
    /// end date label
    @IBOutlet weak var endDateButton: UIButton!
    
    /// test to set the duration
    var test: Test!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
