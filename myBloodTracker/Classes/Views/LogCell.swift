//
//  LogCell.swift
//  myBloodTracker
//
//  Created by harleyguru on 7/28/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit

class LogCell: UITableViewCell {
    
    /// returns cell identifier of this cell, its class name
    static var identifier: String {
        get {
            return String.className(aClass: self)
        }
    }
    
    /// icon view to show calender date
    @IBOutlet weak var iconView: UIImageView!
    
    /// test name label
    @IBOutlet weak var nameLabel: UILabel!
    
    /// date label taken this test
    @IBOutlet weak var dateLabel: UILabel!
    
    /// time label taken this test
    @IBOutlet weak var timeLabel: UILabel!
    
    /// test value label
    @IBOutlet weak var valueLabel: UILabel!
    
    /// test unit label
    @IBOutlet weak var unitLabel: UILabel!
    
    /// stored property for log to display in this cell
    var _log: Log!
    
    /// computed property for log, here cell init its contents from its log data
    var log: Log {
        get {
            return _log
        }
        
        set {
            _log = newValue
            let test = Test(id: _log.testId)
            var unit: Unit!
            
            do {
                try test.query()
                try test.queryUnit()
                try test.queryGroup()
                unit = test.activeUnit
                try unit!.query()
            } catch {
                self.nameLabel.text = nil
            }
            self.nameLabel.text = test.name!
            self.unitLabel.text = unit!.name!
            self.valueLabel.textColor = UIColor(red:0.26, green:0.61, blue:0.26, alpha:1.0)

            var attribute = [NSForegroundColorAttributeName: UIColor(red:0.26, green:0.61, blue:0.26, alpha:1.0)]
            
            if _log.value < test.min || _log.value > test.max {
                attribute = [NSForegroundColorAttributeName: UIColor.red]
            }
            
            let attributedValueString = NSMutableAttributedString(string: "\(_log.value!)", attributes: attribute)
            
            self.valueLabel.attributedText = attributedValueString
            
            let date = Date(timeIntervalSince1970: _log.datetime)
            let calendar = NSCalendar.current
            let components = calendar.dateComponents([.year, .month, .day, .hour, .minute, .era], from: date)
            self.dateLabel.text = "\(components.day!)"
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            self.timeLabel.text = dateFormatter.string(from: date as Date)
            
            self.accessoryType = .disclosureIndicator
            
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
