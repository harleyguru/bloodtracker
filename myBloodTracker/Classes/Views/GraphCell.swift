//
//  GraphCell.swift
//  myBloodTracker
//
//  Created by harleyguru on 6/26/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit
import SwiftCharts

class GraphCell: UITableViewCell {
    
    
    /// container view for all components
    @IBOutlet weak var container: UIView!

    /// title label to display for this graph cell
    @IBOutlet weak var testTitleLabel: UILabel!
    
    /// unit label to display for this graph cell
    @IBOutlet weak var unitLabel: UILabel!
    
    /// graph container view
    @IBOutlet weak var chartView: UIView!
    
    /// color bar container view
    @IBOutlet weak var colorBarView: UIView!
    
    /// label for min value of this test
    @IBOutlet weak var minLabel: UILabel!
    
    /// label for max value of this test
    @IBOutlet weak var maxLabel: UILabel!
    
    /// button to go to detail page
    @IBOutlet weak var infoButton: UIButton!
    
    /// parent view controller of this cell
    weak var graphViewController: GraphPageVC!
    
    /// collection of popups
    var popups: [UIView] = []
    
    /// selected chart point view
    var selectedView: ChartPointView?
    
    /// color bar height
    let colorBarHeight: CGFloat = 10
    
    /// chart property to retain graph instance
    var chart: Chart?
    
    static var identifier: String {
        get {
            return String.className(aClass: self)
        }
    }
    
    /// backbone test
    var _test: Test?
    
    /// test property
    var test: Test? {
        get {
            return _test
        }
        
        set {
            
            _test = newValue
            _logs = nil
            
            if self.testTitleLabel != nil {
                self.testTitleLabel.text = _test?.name
            }
            
            DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
                
                if self.test?.units == nil || self.test?.units.count == 0 {
                    do {
                        try self.test?.queryUnit()
                    } catch {
                        return
                    }
                }
                
                DispatchQueue.main.async {
                    self.unitLabel.text = self._test?.activeUnit?.name
                    self.minLabel.text = "\((self._test?.min)!)"
                    self.maxLabel.text = "\((self._test?.max)!)"
                    
                    if (self.logs?.count)! > 0 {
                        self.drawGraph()
                    }
                }
            }
            
        }
    }
    
    /// backbone logs of this test
    var _logs: [Log]?
    
    /// logs of this test
    var logs: [Log]? {
        get {
            if _logs == nil {
                _logs = try! self.test?.queryLogs()
            }
            
            return _logs
        }
    }
    
    /// get max value for y axis through all log values of this test
    var yMax: Double? {
        get {
            if let logs = self.logs {
                var maxValue: Double! = 0
                for log in logs {
                    if maxValue < log.value {
                        maxValue = log.value
                    }
                }
                
                return max((self.test?.max)!, maxValue!)
            }
            
            return 0
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Action
    
    /// action when user taps on info button
    ///
    /// - parameter _: info button
    @IBAction func onInfo(_: AnyObject) {
        self.graphViewController.performSegue(withIdentifier: "showGraphInfo", sender: self)
    }
    
    /// action called when user tapped on chart view
    ///
    /// - parameter gestureRecognizer: tap gesture recognizer bound with the chart point view
    func onChart(gestureRecognizer: UITapGestureRecognizer!) {
        for p in self.popups {p.removeFromSuperview()}
        self.selectedView?.selected = false
    }
    
    // MARK: - Helper
    
    /// draw graph in the cell
    func drawGraph() {
        
        // setup datetime display format
        let displayFormatter = DateFormatter()
        displayFormatter.dateFormat = "MM/dd/yy"
        
        // setup chart points (datetime, value)
        var chartPoints = [ChartPoint]()
        let logs = (self.test?.logs)!
        for log in logs {
            
            let date = Date(timeIntervalSince1970: log.datetime)
            let chartPoint = ChartPoint(x: ChartAxisValueDate(date: date, formatter: displayFormatter), y: ChartAxisValueDouble(log.value))
            
            if date.timeIntervalSince1970 <= (self.test?.endtime)! && date.timeIntervalSince1970 >= (self.test?.starttime!)! {
                // if this test log has been taken in valid duration only, add it to graph
                chartPoints.append(chartPoint)
            }
        }
        chartPoints = chartPoints.sorted { (point1: ChartPoint, point2: ChartPoint) -> Bool in
            
            let xValue1 = point1.x as! ChartAxisValueDate
            let xValue2 = point2.x as! ChartAxisValueDate
            
            if xValue1.date.compare(xValue2.date) == .orderedAscending {
                return true
            } else {
                return false
            }
        }
        
        // setup x axis values - datetime
        var xValues = [ChartAxisValueDate]()
        let interval = (self.test?.endtime)! - (self.test?.starttime)! // interval between starttime and endtime
        for i in 0...10 {
            let value = (self.test?.starttime)! + (interval / Double(10)) * Double(i)
            let date = Date(timeIntervalSince1970: value)
            let labelSettings = ChartLabelSettings(font: ExamplesDefaults.labelFont, rotation: 45, rotationKeep: .top)
            xValues.append(ChartAxisValueDate(date: date, formatter: displayFormatter, labelSettings: labelSettings))
        }
        xValues.last?.hidden = true
        
        // setup y axis values - value
        var yValues = [ChartAxisValueDouble]()
        for i in 0...5 {
            let value = (self.yMax! / Double(5)) * Double(i)
            yValues.append(ChartAxisValueDouble(value, labelSettings: ChartLabelSettings(fontColor: .clear)))
        }
        
        // setup settings for axis labels
        let labelSettings = ChartLabelSettings(font: UIFont.systemFont(ofSize: 12))
        
        // create axis models with axis values and axis title
        let xModel = ChartAxisModel(axisValues: xValues, axisTitleLabel: ChartAxisLabel(text: "", settings: labelSettings))
        let yModel = ChartAxisModel(axisValues: yValues, axisTitleLabel: ChartAxisLabel(text: "", settings: labelSettings.defaultVertical()))
        
        // setup frame for this chart
        let chartFrame = ExamplesDefaults.chartFrame(self.chartView.bounds)
        
        // setup color bar
        let colorBar = ColorBar(frame: self.colorBarView.bounds)
        colorBar.tag = 10
        self.colorBarView.viewWithTag(10)?.removeFromSuperview()
        self.colorBarView.addSubview(colorBar)
        
        // setup settings for chart coordinate space
        let chartSettings = ChartSettings()
        chartSettings.axisStrokeWidth = 0
        chartSettings.top = 10
        chartSettings.trailing = 10
        chartSettings.bottom = -21
        chartSettings.labelsWidthY = 30
        chartSettings.axisTitleLabelsToLabelsSpacing = 0
        
        // setup coordinate space based on the current configuration - this will take some time, so you consider to run this code in background thread
        let coordsSpace = ChartCoordsSpaceLeftBottomSingleAxis(chartSettings: chartSettings, chartFrame: chartFrame, xModel: xModel, yModel: yModel)
        let (xAxis, yAxis, innerFrame) = (coordsSpace.xAxis, coordsSpace.yAxis, coordsSpace.chartInnerFrame)
        
        // adjust min, max value label location to be displayed in y axis
        let maxValue = CGFloat(self.yMax!)
        let h = self.colorBarView.frame.size.height
        let yMin = (h / maxValue) * CGFloat((self.test?.min)!)
        let yMax = (h / maxValue) * CGFloat((self.test?.max)!)
        var frame = self.minLabel.frame
        frame.origin.y = h - yMin - (frame.size.height / 2)
        let minY = frame.origin.y
        self.minLabel.frame = frame
        frame = self.maxLabel.frame
        frame.origin.y = h - yMax - (frame.size.height / 2)
        let maxY = frame.origin.y
        self.maxLabel.frame = frame
        if self.minLabel.text == "0.0" || minY - maxY < 10 {
            self.minLabel.isHidden = true
        }
        
        // adjust min, max value location of color bar
        colorBar.min = Double(yMin)
        colorBar.max = Double(yMax)
        
        // create layer with graph line
        let lineModel = ChartLineModel(chartPoints: chartPoints, lineColor: UIColor(red:0.25, green:0.49, blue:0.85, alpha:0.6), lineWidth: 2, animDuration: 0, animDelay: 0)
        let chartPointsLineLayer = ChartPointsLineLayer(xAxis: xAxis, yAxis: yAxis, innerFrame: innerFrame, lineModels: [lineModel])
        
        // creates custom view for each chartpoint
        let myCustomViewGenerator = {[weak self] (chartPointModel: ChartPointLayerModel, layer: ChartPointsLayer, chart: Chart) -> UIView? in
            
            guard let wself = self else {
                return nil
            }
            
            let (chartPoint, screenLoc) = (chartPointModel.chartPoint, chartPointModel.screenLoc)
            let value = (chartPoint.y as! ChartAxisValueDouble).scalar
            let pointView = ChartPointView(center: screenLoc, diameter: 14, cornerRadius: 7, borderWidth: 3)
            
            if value > (wself.test?.max)! || value < (wself.test?.min)! {
                pointView.isRangeIn = false
            } else {
                pointView.isRangeIn = true
            }
            
            pointView.viewTapped = {view in
                
                for p in wself.popups {p.removeFromSuperview()}
                wself.selectedView?.selected = false
                
                // width and height of chart info view to pop up
                let width: CGFloat = Env.iPad ? 200 : 100
                let height: CGFloat = Env.iPad ? 40 : 20
                
                let x: CGFloat = {
                    let attempt = screenLoc.x
                    
                    let rightBound = chart.bounds.size.width - 10
                    
                    if attempt + width > rightBound {
                        return screenLoc.x - width
                    }
                    
                    return attempt
                }()
                
                let y: CGFloat = {
                    let attempt = screenLoc.y - (view.frame.height / 2) - height
                    
                    let topBound = chart.bounds.origin.y
                    
                    if attempt < topBound {
                        return screenLoc.y + (view.frame.height / 2)
                    }
                    
                    return attempt
                }()
                
                let frame = CGRect(x: x, y: y, width: width, height: height)
                
                let bubbleView = ChartInfoView(frame: frame, borderColor: view.backgroundColor!)
                chart.addSubview(bubbleView)
                
                bubbleView.transform = CGAffineTransform(scaleX: 0, y: 0).concatenating(CGAffineTransform(translationX: 0, y: 50))
                let infoView = UILabel(frame: CGRect(x: 0, y: 4, width: width, height: height - 8))
                
                if view.isRangeIn {
                    infoView.textColor = UIColor(red:0.22, green:0.52, blue:0.22, alpha:1.0) // range in
                } else {
                    infoView.textColor = UIColor(red:0.71, green:0.02, blue:0.06, alpha:1.0) // range out
                }

                infoView.backgroundColor = UIColor.clear
                infoView.text = "\(chartPoint)"
                infoView.font = ExamplesDefaults.fontWithSize(Env.iPad ? 14 : 12)
                infoView.textAlignment = NSTextAlignment.center
                
                bubbleView.addSubview(infoView)
                wself.popups.append(bubbleView)
                
                UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions(), animations: {
                    view.selected = true
                    wself.selectedView = view
                    
                    bubbleView.transform = CGAffineTransform.identity
                    }, completion: {finished in})
            }
            
            return pointView
        }
        
        // create layer that uses the view generator
        let myCustomViewLayer = ChartPointsViewsLayer(xAxis: xAxis, yAxis: yAxis, innerFrame: innerFrame, chartPoints: chartPoints, viewGenerator: myCustomViewGenerator, displayDelay: 0, delayBetweenItems: 0)
        
        
        // create layer with guidelines
        let settings = ChartGuideLinesDottedLayerSettings(linesColor: .lightGray, linesWidth: 1)
        let guidelinesLayer = ChartGuideLinesDottedLayer(xAxis: xAxis, yAxis: yAxis, innerFrame: innerFrame, settings: settings)
        
        let chart = Chart(
            frame: chartFrame,
            layers: [
                xAxis,
                yAxis,
                guidelinesLayer,
                chartPointsLineLayer,
                myCustomViewLayer
            ]
        )
        
        chart.view.tag = 10
        self.chartView.viewWithTag(10)?.removeFromSuperview()
        self.chartView.addSubview(chart.view)
        
        // add tap gesture for chart view
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(GraphCell.onChart(gestureRecognizer:)))
        gestureRecognizer.delegate = self
        chart.view.addGestureRecognizer(gestureRecognizer)
        
        self.chart = chart
        
    }
    
}

extension GraphCell {
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view! == self.chart?.view {
            return true
        }
        
        return false
    }
}
