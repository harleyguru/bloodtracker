//
//  LimitCell.swift
//  myBloodTracker
//
//  Created by harleyguru on 10/19/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit

/// cell for minimum/maximum limit setting in UnitAndLimitVC
class LimitCell: UITableViewCell {
    
    /// reuse identifier for this cell
    static var identifier: String {
        get {
            return String.className(aClass: self)
        }
    }
    
    /// show minimum or maximum label
    @IBOutlet weak var limitNameLabel: UILabel!
    
    /// text field for limit input
    @IBOutlet weak var limitField: UITextField!
    
    /// label to display the current unit selected for the test
    @IBOutlet weak var unitLabel: UILabel!
    
    /// index path row of this cell, it will be used to identify if this cell is for minimum or maximum, this shoulbe set before setting test in parent view controller
    var row: Int = 0
    
    /// parent view controller of this cell: UnitAndLimitVC, this should be set before setting test in parent view controller
    weak var limitViewController: UnitAndLimitVC!
    
    /// backbone of test property
    var _test: Test!
    
    /// test for this limit cell
    var test: Test! {
        get {
            return _test
        }
        
        set {
            _test = newValue
            
            if self.row == 0 { // maximum
                
                self.limitNameLabel.text = "Maximum"
                self.limitField.text = "\((self.test.max)!)"
                self.limitField.placeholder = "Set maximum value"
                self.limitField.inputAccessoryView = self.limitViewController.toolbar
                self.limitViewController.maxTextField = self.limitField
                self.limitField.tag = self.row
                self.unitLabel.text = self.test.activeUnit?.name
                
            } else { // minimum
                
                self.limitNameLabel.text = "Minimum"
                self.limitField.text = "\((self.test.min)!)"
                self.limitField.inputAccessoryView = self.limitViewController.toolbar
                self.limitField.tag = self.row
                self.limitViewController.minTextField = self.limitField
                self.limitField.placeholder = "Set minimum value"
                self.unitLabel.text = self.test.activeUnit?.name
                
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Action
    
    /**
     action when user taps on done button for inputing value in toolbar
     
     - author: harleyguru
     
     - parameter _: sender
     */
    @IBAction func onDone(_: AnyObject) {
        
        if self.row == 0 { // this is cell for maximum
            self.test.max = Double(self.limitField.text!)
        } else { // this is cell for minimum
            self.test.min = Double(self.limitField.text!)
        }

        self.limitField.resignFirstResponder()
    }
    
    /**
     action when user taps on cancel button for inputing value in toolbar
     
     - author: harleyguru
     
     - parameter _: sender
     */
    @IBAction func onCancel(_: AnyObject) {

        do {
            try self.test.query()
            if self.row == 0 { // maximum cell
                self.limitField.text = "\(self.test.max)!"
            } else { // minimum cell
                self.limitField.text = "\(self.test.min)!"
            }
        } catch {
            
        }
        self.limitField.resignFirstResponder()
    }

}

extension LimitCell: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text = textField.text! as NSString
        let newText = text.replacingCharacters(in: range, with: string)
        
        if self.row == 0 { // maximum field
            self.test.max = Double(newText)
            if self.test.max != nil {
                self.limitViewController.doneButton.isEnabled = true
            } else {
                self.limitViewController.doneButton.isEnabled = false
                self.limitField.text = nil
            }
        } else { // minimum field
            self.test.min = Double(newText)
            if self.test.max != nil {
                self.limitViewController.doneButton.isEnabled = true
            } else {
                self.limitViewController.doneButton.isEnabled = false
                self.limitField.text = nil
            }
        }
        
        return true
    }
}

