//
//  ChartPointView.swift
//  myBloodTracker
//
//  Created by harleyguru on 10/13/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit


/// Custom view used as chart point
class ChartPointView: UIView {
    
    /// center location of this view in the screen
    fileprivate let targetCenter: CGPoint
    
    /// indicate whether the value of this view is in range or not
    var isRangeIn: Bool = false {
        didSet {
            if isRangeIn {
                self.backgroundColor = UIColor(red:0.30, green:0.77, blue:0.48, alpha:1.0)
            } else {
                self.backgroundColor = UIColor(red:0.98, green:0.51, blue:0.51, alpha:1.0)
            }
        }
    }
    
    /// closure to be called when the user taps on this view
    open var viewTapped: ((ChartPointView) -> ())?

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    /// decorate the view according the selection status of this view
    var selected: Bool = false {
        didSet {
            if selected {
                if isRangeIn {
                    self.backgroundColor = UIColor(red:0.22, green:0.52, blue:0.22, alpha:1.0)
                } else {
                    self.backgroundColor = UIColor(red:0.71, green:0.02, blue:0.06, alpha:1.0)
                }
            } else {
                self.isRangeIn = self.isRangeIn ? true : false
            }
        }
    }
    
    /// convinience initializer
    ///
    /// - parameter center:       center location of this view
    /// - parameter diameter:     diameter of this view
    /// - parameter cornerRadius: corner radius for this view
    /// - parameter borderWidth:  border width of this view
    ///
    /// - returns: ChartPointView initialized
    public init(center: CGPoint, diameter: CGFloat, cornerRadius: CGFloat, borderWidth: CGFloat) {
        
        self.targetCenter = center
        
        super.init(frame: CGRect(x: center.x - diameter / 2, y: center.y - diameter / 2, width: diameter, height: diameter))
        
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = UIColor.white.cgColor
        
        let c = UIColor(red: 1, green: 1, blue: 1, alpha: 0.85)
        self.layer.backgroundColor = c.cgColor
        
        self.isUserInteractionEnabled = true
    }
    
    required public init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override open func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        viewTapped?(self)
    }

}
