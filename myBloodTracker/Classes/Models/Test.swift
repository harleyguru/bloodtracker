//
//  Test.swift
//  myBloodTracker
//
//  Created by harleyguru on 7/21/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit

/// enumeration for duration of test logs
///
/// - week:     user selected one week as a duration
/// - month:    user selected one momth as a duration
/// - halfYear: user selected on 6 months as a duration
/// - year:     user selected one year as a duration
/// - any:      user selected any as a duratoin
/// - all:      user selected entire duration
enum Duration: Int64 {
    case week = 1
    case month = 2
    case halfYear = 3
    case year = 4
    case any = 5
    case all = 6
}

/// represents tests table
class Test: NSObject {

    /// _id field of this test
    var id: Int64!
    
    /// name field of this test
    var name: String!
    
    /// abbreviation field of this test
    var abbreviation: String?
    
    /// info field of this test
    var info: String?
    
    /// min field of this test
    var min: Double!
    
    /// max field of this test
    var max: Double!
    
    /// value of lab test user has done against this test
    var value: Double?
    
    /// all available units of this test
    var units: [Unit] = [Unit]()
    
    /// all groups this test belongs to
    var groups: [Group] = [Group]()
    
    /// indicate if log for this test has been changed by user
    var edited: Bool = false
    
    /// get and set index of active unit in units array of this test, 
    /// - note: setting never affect to db record
    var activeUnitIndex: Int? {
        
        get {
            for (index, unit) in self.units.enumerated() {
                if unit.active {
                    return index
                }
            }
            
            return nil
        }
        
        set {
            for (index, unit) in units.enumerated() {
                if index == newValue! {
                    unit.active = true
                } else {
                    unit.active = false
                }
            }
        }
    }
    
    /// get/set active unit of this test
    /// - note: setting never affect to db record
    var activeUnit: Unit? {
        
        get {
            for unit in units {
                if unit.active {
                    return unit
                }
            }
            
            return nil
        }
        
        set {
            for unit in units {
                if unit.id == newValue!.id {
                    unit.active = true
                } else {
                    unit.active = false
                }
            }
        }
        
    }
    
    /// backbone logs of this test
    var _logs: [Log]?
    
    /// logs of this test
    var logs: [Log]? {
        get {
            if _logs == nil {
                _logs = try! self.queryLogs()
            }
            
            return _logs
        }
    }
    
    /// backbone property for duration, it indicates all duration in default
    var _duration: Int64 = 6
    
    /// duration to display logs of this test
    var duration: Duration {
        get {
            switch _duration {
            case 1:
                return .week
            case 2:
                return .month
            case 3:
                return .halfYear
            case 4:
                return .year
            case 5:
                return .any
            default:
                return .all
            }
        }
        
        set {
            _duration = newValue.rawValue
        }
    }

    /// backbone of starttime for duration of this test
    var _starttime: Double?
    
    /// starttime for duration fo this test, if user never selected any duration in settings, it should be calculated from duration automatically
    var starttime: Double? {
        get {
            switch self.duration {
            case .any:
                if _starttime == 0 {
                    _starttime = Date().timeIntervalSince1970
                }
            case .all:
                
                let logs = self.logs?.sorted(by: { (log1, log2) -> Bool in
                    if log1.datetime < log2.datetime { // sort on asc of datetime
                        return true
                    } else {
                        return false
                    }
                })
                
                if (logs?.count)! > 0 { // there are logs for this test
                    _starttime = logs?.first?.datetime
                } else { // there are no logs for this test
                    _starttime = Date().timeIntervalSince1970
                }
                
            default:
                let (starttime, _) = Date.datetimeFor(duration: self.duration)
                _starttime = starttime
            }
            
            return _starttime
        }
        
        set {
            _starttime = newValue
        }
    }
    
    /// backbone of endtime for duration of this test
    var _endtime: Double?
    
    /// endtime for duration fo this test, if user never selected any duration in settings, it should be calculated from duration automatically
    var endtime: Double? {
        get {
            switch self.duration {
            case .any:
                if _endtime == 0 {
                    _endtime = Date().timeIntervalSince1970
                }
            default:
                let (_, endtime) = Date.datetimeFor(duration: self.duration)
                _endtime = endtime
            }
            return _endtime
        }
        
        set {
            _endtime = newValue
        }
    }
    
    override init() {
        
        // do here additional initialization
        
        super.init()
    }
    
    /**
     init test with id
     
     - author: harleyguru
     
     - parameter id: test id
     
     - returns: void
     */
    convenience init(id: Int64) {
        self.init()
        
        self.id = id
    }
    
    /**
     init test
     
     - author: harleyguru
     
     - parameter id:           id of this test
     - parameter name:         name of this test
     - parameter info:         info of this test
     - parameter abbreviation: abbreviation of this test
     - parameter min:          min test value of this test
     - parameter max:          max test value of this test
     - parameter duration:     duration to display logs of this test in the graph
     - parameter starttime:    starttime to display logs of this test in the graph
     - parameter endtime:      endtime to display logs of this test in the graph
     
     - returns: void
     */
    convenience init(id: Int64, name: String, abbreviation: String?, info: String?, min: Double, max: Double, duration: Int64?, starttime: Double?, endtime: Double?) {

        self.init()
        
        self.id = id
        self.name = name
        self.abbreviation = abbreviation
        self.info = info
        self.min = min
        self.max = max
        _duration = duration!
        _starttime = starttime
        _endtime = endtime
    }
    
    /**
     init test
     
     - author: harleyguru
     
     - parameter name:         name of this test
     - parameter info:         info of this test
     - parameter abbreviation: abbreviation of this test
     - parameter min:          min test value of this test
     - parameter max:          max test value of this test
     - parameter duration:     duration to display logs of this test in the graph
     - parameter starttime:    starttime to display logs of this test in the graph
     - parameter endtime:      endtime to display logs of this test in the graph
     
     - returns: void
     */
    convenience init(name: String, abbreviation: String?, info: String?, min: Double, max: Double, duration: Int64?, starttime: Double?, endtime: Double?) {
        
        self.init()
        
        self.name = name
        self.abbreviation = abbreviation
        self.info = info
        self.min = min
        self.max = max
        _duration = duration!
        _starttime = starttime
        _endtime = endtime
    }
    
    /**
     query all tests
     
     you can get array with all tests sorted by alphabetically
     
     - author: harleyguru
        
     - throws: DBOpenError, DBOperationError for db open fail and query fail
     - returns: [Test] for successful query
     */
    static func queryAll() throws -> [Test] {
        
        let query = "select * from tests order by name;"
        
        var statement: OpaquePointer? = nil
        
        defer {
            sqlite3_finalize(statement)
            SQLiteManager.sharedManager.close()
        }
        
        do {
            try SQLiteManager.sharedManager.open()
        } catch {
            
            #if DEBUG
                print("***** could not open database! error: \(SQLiteManager.sharedManager.errorMessage())")
            #endif
            throw SQLiteError.open
        }
        
        if sqlite3_prepare_v2(SQLiteManager.sharedManager.db, query, -1, &statement, nil) == SQLITE_OK {
            
            #if DEBUG
                print(">>>>> query successfully")
            #endif
            
            var result = [Test]()
            var test: Test!
            
            while sqlite3_step(statement) == SQLITE_ROW { // queried successfully
                
                let id = sqlite3_column_int64(statement, 0)
                
                let name = sqlite3_column_text(statement, 1)
                let nameString = String(cString: name!)
                
                let abbreviation = sqlite3_column_text(statement, 2)
                var abbreviationString: String? = nil
                if abbreviation != nil {
                    abbreviationString = String(cString: abbreviation!)
                }
                
                
                let info = sqlite3_column_text(statement, 3)
                var infoString: String? = nil
                if info != nil {
                    infoString = String(cString: info!)
                }
                
                let min = sqlite3_column_double(statement, 4)
                let max = sqlite3_column_double(statement, 5)
                let duration = sqlite3_column_int64(statement, 6)
                let starttime = sqlite3_column_double(statement, 7)
                let endtime = sqlite3_column_double(statement, 8)
                
                let computedDuration = duration == 0 ? 6 : duration
                
                test = Test(id: id, name: nameString, abbreviation: abbreviationString, info: infoString, min: min, max: max, duration: computedDuration, starttime: starttime, endtime: endtime)
                result.append(test)
            }
            
            return result
            
        }
        
        #if DEBUG
            print("***** failed query for test! error: \(SQLiteManager.sharedManager.errorMessage())")
        #endif
        
        throw SQLiteError.operation
    }
    
    /**
     query test with a specific id
     
     you can get updated object with queried values.
     In the case no result, you need to check name property
     
     - author: harleyguru
     
     - throws: DBOpenError if db opening fails, DBOperationError if inserting fails
     */
    func query() throws {
        
        let query = "select * from tests where _id=?;"
        
        var statement: OpaquePointer? = nil
        
        defer {
            sqlite3_finalize(statement)
            SQLiteManager.sharedManager.close()
        }
        
        do {
            try SQLiteManager.sharedManager.open()
        } catch {
            
            #if DEBUG
                print("***** could not open database! error: \(SQLiteManager.sharedManager.errorMessage())")
            #endif
            throw SQLiteError.open
        }
        
        if sqlite3_prepare_v2(SQLiteManager.sharedManager.db, query, -1, &statement, nil) == SQLITE_OK {
            
            #if DEBUG
                print(">>>>> query successfully")
            #endif
            
            sqlite3_bind_int64(statement, 1, self.id) // binding parameter is non-zero index bassis, so the index of first parameter is 1
            
            if sqlite3_step(statement) == SQLITE_ROW { // queried successfully
                
                // get column values from statement
                let name = sqlite3_column_text(statement, 1)
                let nameString = String(cString: name!)
                
                let abbreviation = sqlite3_column_text(statement, 2)
                var abbreviationString: String? = nil
                if abbreviation != nil {
                    abbreviationString = String(cString: abbreviation!)
                }
                
                let info = sqlite3_column_text(statement, 3)
                var infoString: String? = nil
                if info != nil {
                    infoString = String(cString: info!)
                }
                
                let min = sqlite3_column_double(statement, 4)
                let max = sqlite3_column_double(statement, 5)
                let duration = sqlite3_column_int64(statement, 6)
                let starttime = sqlite3_column_double(statement, 7)
                let endtime = sqlite3_column_double(statement, 8)
                
                // update properties of this test
                self.name = nameString
                self.abbreviation = abbreviationString
                self.info = infoString
                self.min = min
                self.max = max
                _duration = duration == 0 ? 6 : duration
                self.starttime = starttime
                self.endtime = endtime
                
            } else { // no test with that id
                
                #if DEBUG
                    print(">>>>> no result")
                #endif
            }
            
            return
        }
        
        #if DEBUG
            print("***** failed query for test! error: \(SQLiteManager.sharedManager.errorMessage())")
        #endif
        
        throw SQLiteError.operation
    }
    
    /**
     query unit information of this test and update properties with information in one shot
     
     - author: harleyguru
     
     - throws: DBOpenError if db opening fails, DBOperationError if query fails
     */
    func queryUnit() throws {
        let query = "select tid, tname, active, uid, uname " +
                    "from (" +
                        "select tu.tid as tid, tu.tname as tname, tu.activate as active, units._id as uid, units.name as uname " +
                        "from ( " +
                            "select tests._id as tid, tests.name as tname, test_unit.unit_id as uid, test_unit.activate as activate " +
                            "from tests join test_unit on tests._id = test_unit.test_id " +
                        ") as tu join units on tu.uid = units._id " +
                    ") as tunit " +
                    "where tunit.tid = ? " +
                    "order by uid;"
        
        var statement: OpaquePointer? = nil
        
        defer {
            sqlite3_finalize(statement)
            SQLiteManager.sharedManager.close()
        }
        
        do {
            try SQLiteManager.sharedManager.open()
        } catch {
            
            #if DEBUG
                print("***** could not open database! error: \(SQLiteManager.sharedManager.errorMessage())")
            #endif
            throw SQLiteError.open
        }
        
        if sqlite3_prepare_v2(SQLiteManager.sharedManager.db, query, -1, &statement, nil) == SQLITE_OK {
            
            #if DEBUG
                print(">>>>> queryUnit successfully")
            #endif
            
            sqlite3_bind_int64(statement, 1, self.id)
            
            self.units.removeAll()
            
            while sqlite3_step(statement) == SQLITE_ROW { // queried successfully
                
                // get column values from statement
                let active = sqlite3_column_int64(statement, 2) // get active status of unit
                let unitId = sqlite3_column_int64(statement, 3) // get unit id
                
                let unitName = sqlite3_column_text(statement, 4) // get unit name
                let unitNameString = String(cString: unitName!)
                
                // update properties of this test
                let unit = Unit(id: unitId, name: unitNameString, active: active)
                self.units.append(unit)
                
            }
            
            return
        }
        
        #if DEBUG
            print("***** failed queryUnit for test! error: \(SQLiteManager.sharedManager.errorMessage())")
        #endif
        
        throw SQLiteError.operation
    }
    
    /**
     query group information of this test and update properties with information in one shot
     
     - author: harleyguru
     
     - throws: DBOpenError if db opening fails, DBOperationError if query fails
     */
    func queryGroup() throws {
        let query = "select tid, tname, gid, gname " +
                    "from ( " +
                        "select tgroup.tid as tid, tgroup.tname as tname, groups._id as gid, groups.name as gname " +
                        "from ( " +
                            "select tests._id as tid, tests.name as tname, test_group.group_id as gid " +
                            "from tests join test_group on tests._id = test_group.test_id " +
                            ") as tgroup join groups on tgroup.gid = groups._id " +
                        ") as tgroup " +
                    "where tgroup.tid = ?;"
        
        var statement: OpaquePointer? = nil
        
        defer {
            sqlite3_finalize(statement)
            SQLiteManager.sharedManager.close()
        }
        
        do {
            try SQLiteManager.sharedManager.open()
        } catch {
            
            #if DEBUG
                print("***** could not open database! error: \(SQLiteManager.sharedManager.errorMessage())")
            #endif
            throw SQLiteError.open
        }
        
        if sqlite3_prepare_v2(SQLiteManager.sharedManager.db, query, -1, &statement, nil) == SQLITE_OK {
            
            #if DEBUG
                print(">>>>> queryGroup successfully")
            #endif
            
            sqlite3_bind_int64(statement, 1, self.id)
            
            self.groups.removeAll()
            
            while sqlite3_step(statement) == SQLITE_ROW { // queried successfully
                
                // get column values from statement
                let groupId = sqlite3_column_int64(statement, 2) // get group id
                
                let groupName = sqlite3_column_text(statement, 3) // get group name
                let groupNameString = String(cString: groupName!)
                
                // update properties of this test
                let group = Group(id: groupId, name: groupNameString)
                self.groups.append(group)
                
            }
            
            return
        }
        
        #if DEBUG
            print("***** failed queryGroup for test! error: \(SQLiteManager.sharedManager.errorMessage())")
        #endif
        
        throw SQLiteError.operation
    }
    
    /**
     query all logs associated with this test
     
     - author: harleyguru
     
     - throws: DBOpenError if db opening fails, DBOperationError if query fails
     */
    func queryLogs() throws -> [Log] {
        
        let query = "select _id, datetime, value from logs where test_id = ?;"
        
        var statement: OpaquePointer? = nil
        
        defer {
            sqlite3_finalize(statement)
            SQLiteManager.sharedManager.close()
        }
        
        do {
            try SQLiteManager.sharedManager.open()
        } catch {
            
            #if DEBUG
                print("***** could not open database! error: \(SQLiteManager.sharedManager.errorMessage())")
            #endif
            throw SQLiteError.open
        }
        
        if sqlite3_prepare_v2(SQLiteManager.sharedManager.db, query, -1, &statement, nil) == SQLITE_OK {
            
            #if DEBUG
                print(">>>>> queryLogs successfully")
            #endif
            
            sqlite3_bind_int64(statement, 1, self.id)
            
            var logs = [Log]()
            
            while sqlite3_step(statement) == SQLITE_ROW { // queried successfully
                
                // get column values from statement
                let id = sqlite3_column_int64(statement, 0)
                let datetime = sqlite3_column_double(statement, 1)
                let value = sqlite3_column_double(statement, 2)

                let log = Log(id: id, test: self.id, value: value, datetime: datetime)
                logs.append(log)
                
            }
            
            return logs
        }
        
        #if DEBUG
            print("***** failed queryLogs! error: \(SQLiteManager.sharedManager.errorMessage())")
        #endif
        
        throw SQLiteError.open
    }
    
    /**
     insert this test to tests table
     
     you will get updated test object with new rowid(this is primary key<_id> in default)
     
     - author: harleyguru
     
     - throws: DBOpenError if db opening fails, DBOperationError if inserting fails
     */
    func insert() throws {
        
        // TODO: insert including duration, starttime, endtime
        let query = "insert into tests(name, abbreviation, info, min, max) values (?, ?, ?, ?, ?);"
        
        var statement: OpaquePointer? = nil
        
        defer {
            // close databse
            sqlite3_finalize(statement)
            SQLiteManager.sharedManager.close()
        }
        
        do { // open database
            try SQLiteManager.sharedManager.open()
        } catch {
            #if DEBUG
                print("***** could not open database! error: \(SQLiteManager.sharedManager.errorMessage())")
            #endif
            throw SQLiteError.open
        }
        
        if sqlite3_prepare_v2(SQLiteManager.sharedManager.db, query, -1, &statement, nil) == SQLITE_OK {
            
            sqlite3_bind_text(statement, 1, self.name, -1, nil)
            
            if let abbreviationValue = self.abbreviation {
                sqlite3_bind_text(statement, 2, abbreviationValue, -1, nil)
            }
            
            if let infoValue = self.info {
                sqlite3_bind_text(statement, 3, infoValue, -1, nil)
            }
            
            sqlite3_bind_double(statement, 4, self.min)
            sqlite3_bind_double(statement, 5, self.max)
            
            if sqlite3_step(statement) == SQLITE_DONE { // inserted successfully
                
                #if DEBUG
                    print(">>>>> inserted successfully")
                #endif
                
                let lastRowId = sqlite3_last_insert_rowid(SQLiteManager.sharedManager.db)
                
                self.id = lastRowId
                
                return
                
            } else { // failed inserting
                
                #if DEBUG
                    print("***** failed inserting a test! error: \(SQLiteManager.sharedManager.errorMessage())")
                #endif
                
                throw SQLiteError.open
            }
            
        }
        
        #if DEBUG
            print("***** failed for inserting a test! error: \(SQLiteManager.sharedManager.errorMessage())")
        #endif
        
        throw SQLiteError.operation
    }
    
    /**
     update test in tests table
     
     you will get updated test object, since this is update, all properties are same as it is
     
     - author: harleyguru
     
     - throws: DBOpenError if db opening fails, DBOperationError if updating fails
     */
    func update() throws {
        
//        let query = "update tests set name = ?, abbreviation = ?, info = ?, min = ?, max = ?, duration = ?, starttime = ?, endtime = ? where _id = ?;"
        let query = "update tests set min = ?, max = ?, duration = ?, starttime = ?, endtime = ? where _id = ?;"
        
        var statement: OpaquePointer? = nil
        
        defer {
            // close databse
            sqlite3_finalize(statement)
            SQLiteManager.sharedManager.close()
        }
        
        do { // open database
            try SQLiteManager.sharedManager.open()
        } catch {
            
            #if DEBUG
                print("***** couldn't open db! error: \(SQLiteManager.sharedManager.errorMessage())")
            #endif
            
            throw SQLiteError.open
        }
        
        if sqlite3_prepare_v2(SQLiteManager.sharedManager.db, query, -1, &statement, nil) == SQLITE_OK {
            
            // binding parameters
//            sqlite3_bind_text(statement, 1, self.name.cString(using: .utf8), -1, nil)
//            
//            if let abbreviationValue = self.abbreviation {
//                sqlite3_bind_text(statement, 2, abbreviationValue.cString(using: .utf8), -1, nil)
//            }
//            
//            if let infoValue = self.info {
//                sqlite3_bind_text(statement, 3, infoValue.cString(using: .utf8), -1, nil)
//            }
            
            sqlite3_bind_double(statement, 1, self.min)
            
            sqlite3_bind_double(statement, 2, self.max)
            
            sqlite3_bind_int64(statement, 3, self.duration.rawValue)
            
            sqlite3_bind_double(statement, 4, self.starttime!)
            
            sqlite3_bind_double(statement, 5, self.endtime!)
            
            sqlite3_bind_int64(statement, 6, self.id)
            
            if sqlite3_step(statement) == SQLITE_DONE { // updated test successfully
                
                #if DEBUG
                    print(">>>>> updated successfully")
                #endif
                
                return;
                
            } else { // failed updating test
                
                #if DEBUG
                    print("***** failed updating a test! error: \(SQLiteManager.sharedManager.errorMessage())")
                #endif
                
                throw SQLiteError.operation
            }
            
        }
        
        #if DEBUG
            print("***** failed for updating a test! error: \(SQLiteManager.sharedManager.errorMessage())")
        #endif
        
        throw SQLiteError.operation
    }
    
    /**
     delete this test in tests table
     
     - author: harleyguru
     
     - throws: DBOpenError if db opening fails, DBOperationError if deleting fails
     */
    func delete() throws {
        let query = "delete from tests where _id = ?;"
        
        var statement: OpaquePointer? = nil
        
        defer {
            // close databse
            sqlite3_finalize(statement)
            SQLiteManager.sharedManager.close()
        }
        
        do { // open database
            try SQLiteManager.sharedManager.open()
        } catch {
            
            #if DEBUG
                print("***** couldn't open db! error: \(SQLiteManager.sharedManager.errorMessage())")
            #endif
            
            throw SQLiteError.open
        }
        
        if sqlite3_prepare_v2(SQLiteManager.sharedManager.db, query, -1, &statement, nil) == SQLITE_OK {
            
            // binding parameters
            sqlite3_bind_int64(statement, 1, self.id)
            
            if sqlite3_step(statement) == SQLITE_DONE { // deleted successfully
                
                #if DEBUG
                    print(">>>>> deleted successfully")
                #endif
                
                return;
                
            } else { // failed deleting
                
                #if DEBUG
                    print("***** failed deleting a test! error: \(SQLiteManager.sharedManager.errorMessage())")
                #endif
                
                throw SQLiteError.operation
            }
            
        }
        
        #if DEBUG
            print("***** failed for deleting a test! error: \(SQLiteManager.sharedManager.errorMessage())")
        #endif
        
        throw SQLiteError.operation
    }
}

