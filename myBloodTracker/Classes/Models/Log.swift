//
//  Log.swift
//  myBloodTracker
//
//  Created by harleyguru on 7/21/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit

/// represent logs table
class Log: NSObject {
    
    /// _id field of this log
    var id: Int64!
    
    /// value field of this log
    var value: Double!
    
    /// datetime field of this log - this is datetime UNIX format timestamp user took the test
    var datetime: Double!
    
    /// test related with this test log
    var testId: Int64!
    
    /// unit related with this test log
    var unitId: Int64!
    
    override init() {
        super.init()
        
        // Do your additional initialization
    }
    
    /**
     init log with its id, test_id, value, datetime, starttime, endtime and duration
     
     - author: harleyguru
     
     - parameter id:        log id
     - parameter test:      test id associated with this
     - parameter unit:      unit id associated with this
     - parameter value:     test value
     - parameter datetime:  datetime stamp this log has been taken

     - returns: no
     */
    convenience init(id: Int64, test: Int64, unit: Int64, value: Double, datetime: Double) {
        self.init()
        
        self.id = id
        self.testId = test
        self.unitId = unit
        self.value = value
        self.datetime = datetime
    }
    
    /**
     init new log in convenience way
     
     - author: harleyguru
     
     - parameter test:     test id associated with this log
     - parameter unit:     unit id associated with this log
     - parameter value:    test value for this log
     - parameter datetime: datetime stamp for UNIX format
     
     - returns:  no
     */
    convenience init(test: Int64, unit: Int64, value: Double, datetime: Double) {
        
        self.init()
        
        self.testId = test
        self.unitId = unit
        self.value = value
        self.datetime = datetime
    }
    
    /**
     init new log in convenience way
     
     - author: harleyguru

     - parameter id: id of this log
     - parameter test:     test id associated with this log
     - parameter value:    test value for this log
     - parameter datetime: datetime stamp for UNIX format
     
     - returns:  no
     */
    convenience init(id: Int64, test: Int64, value: Double, datetime: Double) {
        
        self.init()

        self.id = id
        self.testId = test
        self.value = value
        self.datetime = datetime
    }
    
    /**
     insert this test log to logs table
     
     if insertion is successful, you will get updated log object with new rowid(this is primary key<_id> in default)
     
     - author: harleyguru
     
     - throws: DBOpenError if db opening fails, DBOperationError if inserting fails
     */
    func insert() throws {
        
        let query = "insert into logs(test_id, unit_id, value, datetime) values (?, ?, ?, ?);"
        
        var statement: OpaquePointer? = nil
        
        defer {
            // close databse
            sqlite3_finalize(statement)
            SQLiteManager.sharedManager.close()
        }
        
        do { // open database
            try SQLiteManager.sharedManager.open()
        } catch {
            #if DEBUG
                print("***** could not open database! error: \(SQLiteManager.sharedManager.errorMessage())")
            #endif
            throw SQLiteError.open
        }
        
        if sqlite3_prepare_v2(SQLiteManager.sharedManager.db, query, -1, &statement, nil) == SQLITE_OK {
            
            sqlite3_bind_int64(statement, 1, self.testId)
            sqlite3_bind_int64(statement, 2, self.unitId)
            sqlite3_bind_double(statement, 3, self.value)
            sqlite3_bind_double(statement, 4, self.datetime)

            if sqlite3_step(statement) == SQLITE_DONE { // inserted successfully
                
                #if DEBUG
                    print(">>>>> inserted successfully")
                #endif
                
                let lastRowId = sqlite3_last_insert_rowid(SQLiteManager.sharedManager.db)
                
                self.id = lastRowId
                
                return
                
            } else { // failed inserting
                
                #if DEBUG
                    print("***** failed inserting a log! error: \(SQLiteManager.sharedManager.errorMessage())")
                #endif
                
                throw SQLiteError.operation
            }
            
        }
        
        #if DEBUG
            print("***** failed for inserting a log! error: \(SQLiteManager.sharedManager.errorMessage())")
        #endif
        
        throw SQLiteError.operation
    }
    
    /**
     update this test log to logs table
     
     - author: harleyguru
     
     - throws: DBOpenError if db opening fails, DBOperationError if updating fails
     */
    func update() throws {
        
        let query = "update logs set value = ?, test_id = ?, unit_id = ? where _id = ?;"
        
        var statement: OpaquePointer? = nil
        
        defer {
            // close databse
            sqlite3_finalize(statement)
            SQLiteManager.sharedManager.close()
        }
        
        do { // open database
            try SQLiteManager.sharedManager.open()
        } catch {
            #if DEBUG
                print("***** could not open database! error: \(SQLiteManager.sharedManager.errorMessage())")
            #endif
            throw SQLiteError.open
        }
        
        if sqlite3_prepare_v2(SQLiteManager.sharedManager.db, query, -1, &statement, nil) == SQLITE_OK {
            
            sqlite3_bind_double(statement, 1, self.value)
            sqlite3_bind_int64(statement, 2, self.testId)
            sqlite3_bind_int64(statement, 3, self.unitId)
            sqlite3_bind_int64(statement, 4, self.id)
            
            if sqlite3_step(statement) == SQLITE_DONE { // updated successfully
                
                #if DEBUG
                    print(">>>>> updated successfully")
                #endif
                
                return
                
            } else { // failed updating
                
                #if DEBUG
                    print("***** failed updating a log! error: \(SQLiteManager.sharedManager.errorMessage())")
                #endif
                
                throw SQLiteError.operation
            }
            
        }
        
        #if DEBUG
            print("***** failed for updating a log! error: \(SQLiteManager.sharedManager.errorMessage())")
        #endif
        
        throw SQLiteError.operation
    }
    
    /**
     delete this log in logs table
     
     - author: harleyguru
     
     - throws: DBOpenError if db opening fails, DBOperationError if deleting fails
     */
    func delete() throws {
        let query = "delete from logs where _id = ?;"
        
        var statement: OpaquePointer? = nil
        
        defer {
            // close databse
            sqlite3_finalize(statement)
            SQLiteManager.sharedManager.close()
        }
        
        do { // open database
            try SQLiteManager.sharedManager.open()
        } catch {
            
            #if DEBUG
                print("***** couldn't open db! error: \(SQLiteManager.sharedManager.errorMessage())")
            #endif
            
            throw SQLiteError.open
        }
        
        if sqlite3_prepare_v2(SQLiteManager.sharedManager.db, query, -1, &statement, nil) == SQLITE_OK {
            
            // binding parameters
            sqlite3_bind_int64(statement, 1, self.id)
            
            if sqlite3_step(statement) == SQLITE_DONE { // deleted successfully
                
                #if DEBUG
                    print(">>>>> deleted successfully")
                #endif
                
                return;
                
            } else { // failed deleting
                
                #if DEBUG
                    print("***** failed deleting a test! error: \(SQLiteManager.sharedManager.errorMessage())")
                #endif
                
                throw SQLiteError.operation
            }
            
        }
        
        #if DEBUG
            print("***** failed for deleting a test! error: \(SQLiteManager.sharedManager.errorMessage())")
        #endif
        
        throw SQLiteError.operation
    }
    
    /**
     query all logs
     
     you can get array with all logs sorted by datetime
     
     - author: harleyguru
     
     - throws: DBOpenError, DBOperationError for db open fail and query fail
     - returns: [Log] for successful query
     */
    static func queryAll() throws -> [Log] {
        
        let query = "select _id, test_id, unit_id, value, datetime from logs order by datetime desc;"
        
        var statement: OpaquePointer? = nil
        
        defer {
            sqlite3_finalize(statement)
            SQLiteManager.sharedManager.close()
        }
        
        do {
            try SQLiteManager.sharedManager.open()
        } catch {
            
            #if DEBUG
                print("***** could not open database! error: \(SQLiteManager.sharedManager.errorMessage())")
            #endif
            throw SQLiteError.open
        }
        
        if sqlite3_prepare_v2(SQLiteManager.sharedManager.db, query, -1, &statement, nil) == SQLITE_OK {
            
            #if DEBUG
                print(">>>>> query successfully")
            #endif
            
            var result = [Log]()
            var log: Log!
            
            while sqlite3_step(statement) == SQLITE_ROW { // queried successfully
                
                let id = sqlite3_column_int64(statement, 0)
                
                let testId = sqlite3_column_int64(statement, 1)
                
                let unitId = sqlite3_column_int64(statement, 2)
                
                let value = sqlite3_column_double(statement, 3)
                
                let datetime = sqlite3_column_double(statement, 4)
                
                log = Log(id: id, test: testId, unit: unitId, value: value, datetime: datetime)
                result.append(log)
            }
            
            return result
            
        }
        
        #if DEBUG
            print("***** failed query for log! error: \(SQLiteManager.sharedManager.errorMessage())")
        #endif
        
        throw SQLiteError.operation
    }
    
    /**
     query test associated with this log
     
     This will load test data for this log by test_id of logs table
     
     - author: harleyguru
     
     - throws: DBOpenError, DBOperationError for db open fail and query fail
     - returns: Test for successful query
     */
    func queryTest() throws -> Test {
        
        let query = "select _id, name, abbreviation, info, min, max, duration, starttime, endtime from tests where _id = ?;"
        
        var statement: OpaquePointer? = nil
        
        defer {
            sqlite3_finalize(statement)
            SQLiteManager.sharedManager.close()
        }
        
        do {
            try SQLiteManager.sharedManager.open()
        } catch {
            
            #if DEBUG
                print("***** could not open database! error: \(SQLiteManager.sharedManager.errorMessage())")
            #endif
            throw SQLiteError.open
        }
        
        if sqlite3_prepare_v2(SQLiteManager.sharedManager.db, query, -1, &statement, nil) == SQLITE_OK {
            
            #if DEBUG
                print(">>>>> queryTest for log successfully")
            #endif
            
            sqlite3_bind_int64(statement, 1, self.testId)
            
            var test: Test!
            
            if sqlite3_step(statement) == SQLITE_ROW { // queried successfully
                
                let id = sqlite3_column_int64(statement, 0)
                
                let name = sqlite3_column_text(statement, 1)
                let nameString = String(cString: name!)
                
                let abbreviation = sqlite3_column_text(statement, 2)
                let abbreviationString = String(cString: abbreviation!)
                
                let info = sqlite3_column_text(statement, 3)
                let infoString = String(cString: info!)
                
                let min = sqlite3_column_double(statement, 4)
                
                let max = sqlite3_column_double(statement, 5)
                
                let duration = sqlite3_column_int64(statement, 6)
                
                let starttime = sqlite3_column_double(statement, 7)
                
                let endtime = sqlite3_column_double(statement, 8)
                
                test = Test(id: id, name: nameString, abbreviation: abbreviationString, info: infoString, min: min, max: max, duration: duration, starttime: starttime, endtime: endtime)
                
            }
            
            return test
            
        }
        
        #if DEBUG
            print("***** failed query for test of the log! error: \(SQLiteManager.sharedManager.errorMessage())")
        #endif
        
        throw SQLiteError.operation
    }
}
