//
//  Unit.swift
//  myBloodTracker
//
//  Created by harleyguru on 7/21/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit

/// represent units table
class Unit: NSObject {
    
    /// _id field of this unit
    var id: Int64!
    
    /// name field of this unit
    var name: String!
    
    /// active status field of this unit according any specific test, if its true, it means its default unit for that test currently
    var active: Bool = false
    
    override init() {
        super.init()
    }
    
    /**
     init unit with id
     
     - author: harleyguru
     
     - parameter id: unit id
     
     - returns: void
     */
    convenience init(id: Int64) {
        self.init()
        
        self.id = id
    }
    
    /**
     init unit
     
     - author: harleyguru
     
     - parameter id:     unit id
     - parameter name:   unit name
     - parameter active: active status of unit
     
     - returns: new init
     */
    init(id: Int64, name: String, active: Int64) {
        self.id = id
        self.name = name
        
        if active == 1 {
            self.active = true
        }
    }
    
    /**
     query unit with a specific id
     
     you can get updated object with queried values.
     In the case no result, you need to check name property
     
     - author: harleyguru
     
     - throws: DBOpenError if db opening fails, DBOperationError if inserting fails
     */
    func query() throws {
        
        let query = "select * from units where _id=?;"
        
        var statement: OpaquePointer? = nil
        
        defer {
            sqlite3_finalize(statement)
            SQLiteManager.sharedManager.close()
        }
        
        do {
            try SQLiteManager.sharedManager.open()
        } catch {
            
            #if DEBUG
                print("***** could not open database! error: \(SQLiteManager.sharedManager.errorMessage())")
            #endif
            throw SQLiteError.open
        }
        
        if sqlite3_prepare_v2(SQLiteManager.sharedManager.db, query, -1, &statement, nil) == SQLITE_OK {
            
            #if DEBUG
                print(">>>>> query successfully")
            #endif
            
            sqlite3_bind_int64(statement, 1, self.id) // binding parameter is non-zero index bassis, so the index of first parameter is 1
            
            if sqlite3_step(statement) == SQLITE_ROW { // query successfully
                
                // get column values from statement
                let name = sqlite3_column_text(statement, 1)
                let nameString = String(cString: name!)
                
                // update properties of this unit
                self.name = nameString
                
            } else { // no unit with that id
                
                #if DEBUG
                    print(">>>>> no result")
                #endif
            }
            
            return
        }
        
        #if DEBUG
            print("***** failed query for unit! error: \(SQLiteManager.sharedManager.errorMessage())")
        #endif
        
        throw SQLiteError.operation
    }
}
