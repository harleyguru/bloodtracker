//
//  SQLiteManager.swift
//  myBloodTracker
//
//  Created by harleyguru on 7/18/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

// MARK: - Error types

/**
 SQLite db operation exceptions
 
 - author: harleyguru
 
 - DBOpenError:      error during db connection open
 - DBCopyError:      error occurred during db file copying
 - DBCloseError:     error occurred during db closing
 - DBOperationError: error occurred during db operation such as query, insert, delete and update etc.
 */
enum SQLiteError: Error {
    case open
    case copy
    case close
    case operation
}

/**
 SQLite manager to wrap all SQLite operation

 #### sqlite3 db handler will be stored in this object and this object will work as a singleton
 
 - Create or open a database
 - Insert a row
 - Update a row
 - Delete a row
 - Query the database
 - Handle SQLite errors
 - Close a database
 */
class SQLiteManager: NSObject {
    
    /// singleton sqlite db manager
    static let sharedManager = SQLiteManager()

    /// sqlite3 db handler
    var db: OpaquePointer? = nil
    
    /// sqlite3 db file path, we use /application/documents
    var dbPath: String? = nil
    
    override init() {
        super.init()
    }

    deinit {
        self.close()
    }
  
    // MARK: - Methods
    
    /**
     copy buit-in database into /Documents directory and open a connection
     
     - authoref: harleyguru
     
     - throws: when database opening is failed: SQLiteError.DBOpenError, when database copying has been failed: SQLiteError.DBCopyError
     */
    func open() throws {
        
        let fileManager = FileManager.default
        let documentDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let destinationPath = documentDirectoryPath + "/mybloodtracker.db"
        let sourcePath = Bundle.main.path(forResource: "mybloodtracker", ofType: "db")
        
        self.dbPath = destinationPath
        
        // check existing of db file, and copy db file of bundle to /documents directory
        if !fileManager.fileExists(atPath: destinationPath)
        {
            do {
                try fileManager.copyItem(atPath: sourcePath!, toPath: destinationPath)
            } catch {
                
                throw SQLiteError.copy
            }
        }
        
        if sqlite3_open(dbPath!, &db) != SQLITE_OK {
            throw SQLiteError.open
        }
    }
    
    /**
     get last sqlite3 db error message
     
     - author: harleyguru
     
     - returns: error message from sqlite3 db operation
     */
    func errorMessage() -> String? {
        
        return String(cString: sqlite3_errmsg(db))
    }
    
    /**
     close database connection
     
     - author: harleyguru
     */
    func close() {
        if sqlite3_close(db) == SQLITE_OK {
            
        }
        else {
            #if DEBUG
                print("***** failed closing db connection, please make sure you have finalized statements")
            #endif
        }
    }
}
