//
//  Group.swift
//  myBloodTracker
//
//  Created by harleyguru on 7/21/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit

/// represents groups table
class Group: NSObject {
    
    /// _id field of this group
    var id: Int64!
    
    /// name field of this group
    var name: String!
    
    /**
     init group
     
     - author: harleyguru
     
     - parameter id:   group id
     - parameter name: group name
     
     - returns: new group
     */
    init(id: Int64, name: String) {
        self.id = id
        self.name = name
    }
    
}
