//
//  TestUnit.swift
//  myBloodTracker
//
//  Created by harleyguru on 10/20/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit

/// Represents test_unit table
class TestUnit: NSObject {
    
    /// _id field of this record
    var id: Int64!
    
    /// test_id field of this record
    var testId: Int64!
    
    /// unit_id field of this record
    var unitId: Int64!
    
    /// activate field of this record, if its true, this unit is selected one for test related
    var activate: Bool = false
    
    override init() {
        
        // do here additional initialization
        
        super.init()
    }
    
    /// convenience initializer
    ///
    /// - parameter id:       id field
    /// - parameter testId:   test_id field
    /// - parameter unitId:   unit_id field
    /// - parameter activate: activate field
    ///
    /// - returns: new test_unit object initialized
    convenience init(id: Int64, testId: Int64, unitId: Int64, activate: Bool) {
        self.init()
        
        self.id = id
        self.testId = testId
        self.unitId = unitId
        self.activate = activate
    }
    
    /// init with unique id fields: test_id and unit_id to identify the record in test_unit table
    ///
    /// - parameter testId: test_id
    /// - parameter unitId: unit_id
    ///
    /// - returns: new test_unit object initialized partially
    convenience init(testId: Int64, unitId: Int64) {
        self.init()
        
        self.testId = testId
        self.unitId = unitId
    }
    
    /// query all information for this record
    ///
    /// - note: this will be used for getting full information of test_unit record identified with test_id and unit_id
    /// - throws: SQLiteError when occurs error during operation
    func query() throws {
        let query = "select _id, test_id, unit_id, activate from test_unit where test_id = ? and unit_id = ?;"
        
        var statement: OpaquePointer? = nil
        
        defer {
            sqlite3_finalize(statement)
            SQLiteManager.sharedManager.close()
        }
        
        do {
            try SQLiteManager.sharedManager.open()
        } catch {
            
            #if DEBUG
                print("***** could not open database! error: \(SQLiteManager.sharedManager.errorMessage())")
            #endif
            throw SQLiteError.open
        }
        
        if sqlite3_prepare_v2(SQLiteManager.sharedManager.db, query, -1, &statement, nil) == SQLITE_OK {
            
            #if DEBUG
                print(">>>>> query successfully")
            #endif
            
            sqlite3_bind_int64(statement, 1, self.testId) // binding parameter is non-zero index bassis, so the index of first parameter is 1
            sqlite3_bind_int64(statement, 2, self.unitId) // binding parameter is non-zero index bassis, so the index of first parameter is 1
            
            if sqlite3_step(statement) == SQLITE_ROW { // queried successfully
                
                // get column values from statement
                let id = sqlite3_column_int64(statement, 0)
                let activate = sqlite3_column_int(statement, 3)
                
                // update properties of this test
                self.id = id
                self.activate = (activate == 0 ? false : true)
                
            } else { // no test_unit with that test_id and unit_id
                
                #if DEBUG
                    print(">>>>> no result")
                #endif
            }
            
            return
        }
        
        #if DEBUG
            print("***** failed query for test! error: \(SQLiteManager.sharedManager.errorMessage())")
        #endif
        
        throw SQLiteError.operation
    }
    
    /// update test_unit record with new information updated of this object
    ///
    /// - note: this will be used to update activate field for the test unit
    /// - throws: SQLiteError
    func update() throws {
        let query = "update test_unit set activate = ? where _id = ?;"
        
        var statement: OpaquePointer? = nil
        
        defer {
            // close databse
            sqlite3_finalize(statement)
            SQLiteManager.sharedManager.close()
        }
        
        do { // open database
            try SQLiteManager.sharedManager.open()
        } catch {
            
            #if DEBUG
                print("***** couldn't open db! error: \(SQLiteManager.sharedManager.errorMessage())")
            #endif
            
            throw SQLiteError.open
        }
        
        if sqlite3_prepare_v2(SQLiteManager.sharedManager.db, query, -1, &statement, nil) == SQLITE_OK {
            
            sqlite3_bind_int(statement, 1, self.activate == true ? 1 : 0)
            
            sqlite3_bind_int64(statement, 2, self.id)
            
            if sqlite3_step(statement) == SQLITE_DONE { // updated test_unit successfully
                
                #if DEBUG
                    print(">>>>> updated successfully")
                #endif
                
                return;
                
            } else { // failed updating test_unit
                
                #if DEBUG
                    print("***** failed updating a test! error: \(SQLiteManager.sharedManager.errorMessage())")
                #endif
                
                throw SQLiteError.operation
            }
            
        }
        
        #if DEBUG
            print("***** failed for updating a test! error: \(SQLiteManager.sharedManager.errorMessage())")
        #endif
        
        throw SQLiteError.operation
    }
}
