//
//  DispatchQueue.swift
//  myBloodTracker
//
//  Created by harleyguru on 9/16/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

public extension DispatchQueue {
    
    private static var tokenTracker = [String]()
    
    public class func once(token: String, block: (Void)->Void) {
        objc_sync_enter(self)
        
        defer {
            objc_sync_exit(self)
        }
        
        if tokenTracker.contains(token) {
            return
        }
        
        tokenTracker.append(token)
        
        block()
    }
}
