//
//  NSDate.swift
//  myBloodTracker
//
//  Created by harleyguru on 8/14/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import Foundation

extension Date {
    
    /**
     get date components with year | month | day
     
     - author: harleyguru
     
     - parameter timestamp: timestamp to convert
     */
    static func components(from timestamp: Double) -> DateComponents {
        
        let date = Date(timeIntervalSince1970: timestamp)
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        
        return components
    }
    
    /**
     MMM YYYY format date string from datetime stamp
     
     - author: harleyguru
     
     - parameter timestamp: timestamp since 1970 to convert
     
     - returns: MMM YYYY format date string
     */
    static func yearMonthString(from timestamp: Double) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM YYYY"
        let date = Date(timeIntervalSince1970: timestamp)
        
        return dateFormatter.string(from: date)
    }
    
    /**
     get its string format from date
     
     - author: harleyguru
     
     - parameter date: date to convert into string format
     
     - parameter format: date format string you want
     
     - returns: MMM d, yyyy, h:m a format string representation of date
     */
    static func stringFromDate(date: Date, format: String = "MMM d, yyy, h:mm a") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    /**
     get date from formatted date string
     
     - author: harleyguru
     
     - parameter dateString: MMM yyyy format of date string
     
     - returns: date converted
     */
    static func date(from dateString: String) -> Date {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM yyyy"
        
        return dateFormatter.date(from: dateString)!
    }
    
    
    /// get starttime and endtime timestamp based on specific duration mode
    ///
    /// - parameter duration: duration user specified
    ///
    /// - returns: tuple of starttime and endtime, (starttime, endtime)
    static func datetimeFor(duration: Duration) -> (Double, Double) {

        var starttime = Date()
        let endtime = Date()
        let calendar = Calendar.current
        var components = calendar.dateComponents([.year, .month, .day], from: Date())
        
        switch duration {
        case .week:
            components.day = components.day! - 7
            starttime = calendar.date(from: components)!
        case .month:
            components.month = components.month! - 1
            starttime = calendar.date(from: components)!
        case .halfYear:
            components.month = components.month! - 6
            starttime = calendar.date(from: components)!
        case .year:
            components.year = components.year! - 1
            starttime = calendar.date(from: components)!
        default: break
            
        }
        
        return (starttime.timeIntervalSince1970, endtime.timeIntervalSince1970)
    }
}
