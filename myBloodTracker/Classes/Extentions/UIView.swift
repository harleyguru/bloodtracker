//
//  UIView.swift
//  SlideMenuControllerSwift
//
//  Created by harleyguru on 11/5/15.
//  Copyright © 2015 harleyguru. All rights reserved.
//

import UIKit

extension UIView {
    class func loadNib<T: UIView>(viewType: T.Type) -> T {
        let className = String.className(aClass: viewType)
        return Bundle(for: viewType).loadNibNamed(className, owner: nil, options: nil)!.first as! T
    }
    
    class func loadNib() -> Self {
        return loadNib(viewType: self)
    }
}
