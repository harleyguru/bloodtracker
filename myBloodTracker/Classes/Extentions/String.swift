//
//  StringExtension.swift
//  SlideMenuControllerSwift
//
//  Created by harleyguru on 1/22/15.
//  Copyright (c) 2015 harleyguru. All rights reserved.
//

import Foundation

extension String {
    static func className(aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
    
    func substring(from: Int) -> String {
        return self.substring(from: from)
    }
    
    var length: Int {
        return self.characters.count
    }
}
