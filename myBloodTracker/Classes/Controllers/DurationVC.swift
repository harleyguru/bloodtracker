//
//  DurationVC.swift
//  myBloodTracker
//
//  Created by harleyguru on 10/16/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit

/// View Controller for setting duration of graph to show the logs
class DurationVC: UIViewController {
    
    /// table view for duration setting
    @IBOutlet weak var tableView: UITableView!
    
    /// date picker for start and end date
    @IBOutlet weak var datePicker: UIDatePicker!
    
    /// toolbar as input accessory view of date picker
    @IBOutlet weak var datePickerToolbar: UIToolbar!
    
    /// hidden text field to be used to popup date picker
    @IBOutlet weak var textField: UITextField!
    
    /// test to set duration for
    var test: Test!
    
    /// indicate whether start button is clicked or end button is clicked
    var startClicked = true
    
    /// start date user selected lastly
    var startDate: Date = Date()
    
    /// end date user selected lastly
    var endDate: Date = Date()
    
    /// selected index in duration table, in default user selection is in entire duration mode
    var selectedIndex: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.textField.inputAccessoryView = self.datePickerToolbar
        self.textField.inputView = self.datePicker
        
        switch self.test.duration {
        case .all:
            self.selectedIndex = 0
        case .week:
            self.selectedIndex = 1
        case .month:
            self.selectedIndex = 2
        case .halfYear:
            self.selectedIndex = 3
        case .year:
            self.selectedIndex = 4
        case .any:
            self.selectedIndex = 5
        }
        
        var date = Date(timeIntervalSince1970: self.test.starttime!)
        self.startDate = date
        date = Date(timeIntervalSince1970: self.test.endtime!)
        self.endDate = date
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - Action
    
    /// user clicked on save button in navigation bar after he sets duration for this test
    ///
    /// - parameter _: save button
    @IBAction func onSave(_: AnyObject) {
        
        // save user updated settings to database
        switch self.selectedIndex {
        case 0:
            self.test.duration = .all
        case 1:
            self.test.duration = .week
        case 2:
            self.test.duration = .month
        case 3:
            self.test.duration = .halfYear
        case 4:
            self.test.duration = .year
        case 5:
            self.test.duration = .any
            self.test.starttime = self.startDate.timeIntervalSince1970
            self.test.endtime = self.endDate.timeIntervalSince1970
        default: break
        }
        
        do {
            try self.test.update()
        } catch {
            print("****** error occured during saving duration!")
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    /// user clicked on start date button
    ///
    /// - parameter _: start date button
    @IBAction func onStartDateButton(_: AnyObject) {
        self.textField.becomeFirstResponder()
        self.startClicked = true
        self.selectedIndex = 5
        self.tableView.reloadData()
    }
    
    /// user clicked on end date button
    ///
    /// - parameter _: end date button
    @IBAction func onEndDateButton(_: AnyObject) {
        self.textField.becomeFirstResponder()
        self.startClicked = false
        self.selectedIndex = 5
        self.tableView.reloadData()
    }
    
    /// user clicked on cancel button in date picker tool bar
    ///
    /// - parameter _: cancel button
    @IBAction func onCancel(_: AnyObject) {
        self.textField.resignFirstResponder()
    }
    
    /// user clicked on done button in date picker tool bar
    ///
    /// - parameter _: done button
    @IBAction func onDone(_: AnyObject) {
        self.textField.resignFirstResponder()
        
        if self.startClicked { // user selected start date
            self.startDate = self.datePicker.date
        } else { // user selected end date
            self.endDate = self.datePicker.date
        }
        
        self.tableView.reloadData()
    }
}

extension DurationVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cellIdentifier = "NormalCell"
        
        if indexPath.row == 5 {
            cellIdentifier = "DurationCell"
        }
        
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        
        if cell == nil { // create cell
            cell = UITableViewCell(style: .value1, reuseIdentifier: cellIdentifier)
        }
        
        var duration = ""
        
        if indexPath.row == 0 { // entire duration
            duration = "Entire duration"
        } else if indexPath.row == 1 { // 1 week
            duration = "1 week"
        } else if indexPath.row == 2 { // 1 month
            duration = "1 month"
        } else if indexPath.row == 3 { // 6 months
            duration = "6 months"
        } else if indexPath.row == 4 { // 1 year
            duration = "1 year"
        } else { // start - end
            let durationCell = cell as! DurationCell
            let startDateString = Date.stringFromDate(date: self.startDate, format: "M/d/yy")
            let endDateString = Date.stringFromDate(date: self.endDate, format: "M/d/yy")
            durationCell.startDateButton.setTitle(startDateString, for: .normal)
            durationCell.endDateButton.setTitle(endDateString, for: .normal)
            cell = durationCell
        }
        
        cell?.textLabel?.text = duration
        
        // mark cell selected lastly with check mark
        cell?.accessoryType = self.selectedIndex == indexPath.row ? .checkmark : .none
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        self.selectedIndex = indexPath.row

        self.tableView.reloadData()
    }
}
