//
//  TestSettingsVC.swift
//  myBloodTracker
//
//  Created by harleyguru on 10/20/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit

/// Test settings view controller, here user could change limit and units for each test
class TestSettingsVC: TestBaseVC {
    
    /// indicate whether units/limits has been updated or not in UnitAndLimitVC
    var isUpdated: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.isUpdated {
            self.tableView.reloadData()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.Notificatoin.didUpdateData), object: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Action
    
    /// user clicked on back button on UnitAndLimit page
    ///
    /// - parameter segue: back button
    @IBAction func onBack(_ segue: UIStoryboardSegue) {
        
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showDetail" {
            let navigationVC = segue.destination as! UINavigationController
            let detailVC = navigationVC.topViewController as! UnitAndLimitVC
            detailVC.settingsViewController = self
            detailVC.test = sender as! Test
        }
    }

}

// MARK: - UITableViewDelegate
extension TestSettingsVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: TestCell.identifier, for: indexPath as IndexPath) as! TestCell
        
        var test: Test? = nil
        
        if self.searchController.isActive && self.searchController.searchBar.text != "" { // user is searching
            
            if let filteredTests = self.filteredTests {
                let key = filteredTests.keys.sorted(by: <)[indexPath.section]
                test = filteredTests[key]?[indexPath.row]
            }
        } else { // normal case
            
            if let indexedTests = self.indexedTests {
                let key = indexedTests.keys.sorted(by: <)[indexPath.section]
                test = indexedTests[key]?[indexPath.row]
            }
        }
        
        if let test = test {
            do {
                try test.queryUnit()
            } catch {
                
            }
            cell.test = test
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let test: Test!
        
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        
        if self.searchController.isActive && self.searchController.searchBar.text != "" { // search case
            
            let key = (self.filteredTests?.keys.sorted(by: <)[indexPath.section])!
            test = (self.filteredTests![key])![indexPath.row]
            
        } else { // normal case
            let key = (self.indexedTests?.keys.sorted(by: <)[indexPath.section])!
            test = (self.indexedTests![key])![indexPath.row]
        }
        
        self.performSegue(withIdentifier: "showDetail", sender: test)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.searchController.isActive && self.searchController.searchBar.text != "" {
            
            if let filteredTests = self.filteredTests {
                
                let key = filteredTests.keys.sorted(by: <)[section]
                return (filteredTests[key]?.count)!
            }
            
            return 0
        }
        
        if let indexedTests = self.indexedTests {
            let key = indexedTests.keys.sorted(by: <)[section]
            return (indexedTests[key]?.count)!
        }
        
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if self.searchController.isActive && self.searchController.searchBar.text != "" {
            if let filteredTests = self.filteredTests {
                return filteredTests.keys.count
            }
            
            return 0
        }
        
        if let tests = self.indexedTests {
            return tests.keys.count
        }
        
        return 0
        
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        
        if self.searchController.isActive && self.searchController.searchBar.text != "" { // user is on the search status
            return Array((self.filteredTests?.keys.sorted(by: <))!)
        }
        
        // user is on the raw tests page
        return Array((self.indexedTests?.keys.sorted(by: <))!)
    }
    
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        
        var sections: [String]?
        
        if self.searchController.isActive && self.searchController.searchBar.text != "" { // user is on the search status
            sections = Array((self.filteredTests?.keys.sorted(by: <))!)
        } else { // user is on the raw tests page
            sections = Array((self.indexedTests?.keys.sorted(by: <))!)
        }
        
        return (sections?.index(of: title))!
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if self.searchController.isActive && self.searchController.searchBar.text != "" { // user is on the search status
            return self.filteredTests?.keys.sorted(by: <)[section]
        } else { // user is on the raw tests page
            return self.indexedTests?.keys.sorted(by: <)[section]
        }
    }
}

