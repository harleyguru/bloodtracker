//
//  TestVC.swift
//  myBloodTracker
//
//  Created by harleyguru on 7/29/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit
import HSDatePickerViewController
import PromiseKit
import SVProgressHUD

/// Select test view controller
class TestVC: TestBaseVC {
    
    /// save bar button
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    /// date button user uses to set date for the test log
    @IBOutlet weak var dateButton: UIButton!
    
    /// date picked lastly, its today in default
    var date: Date = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // set current date
        self.dateButton.setTitle(Date.stringFromDate(date: self.date), for: .normal)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "Select Lab Test"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "TestResultVC" {
            if let testResultVC = segue.destination as? TestResultVC {
                testResultVC.test = sender as! Test
            }
        }
    }

    
    // MARK: - Action
    
    /**
     its called action when user come back by clicking cancel button in add lab test screen
     
     - author: harleyguru
     
     - parameter segue: unwind segue
     */
    @IBAction func onCancelResult(_ segue: UIStoryboardSegue) {
        
        self.tableView.reloadData()
    }
    
    /**
     Its called action when the user comes back by clicking done button in add lab test screen
     
     - author: harleyguru
     
     - parameter segue: unwind segue
     */
    @IBAction func onSaveResult(_ segue: UIStoryboardSegue) {

        self.isEditing = true
    }
    
    /**
     Its called when the view controller changed its editing status
     
     - author: harleyguru
     
     - parameter editing:  editing status
     - parameter animated: animation flag during editing status convergence
     */
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        
        self.tableView.isEditing = true
        self.tableView.reloadData()
    }
    
    /**
     action when user clicks on save button after he inputs new test log
     
     - author: harleyguru
     
     - note: here the app will save updated information to database
     
     - parameter _: sender
     */
    @IBAction func onSave(_: AnyObject) {
        
        // save new log to logs table
        let tests: [String: [Test]]?
        if self.searchController.isActive && self.searchController.searchBar.text != "" { // user in search status
            tests = self.filteredTests
        } else { // user in normal status
            tests = self.indexedTests
        }
        
        SVProgressHUD.show()
        
        _ = DispatchQueue.global().promise {
            
            for key in (tests?.keys.sorted(by: <))! {
                for test in (tests![key])! {
                    if test.edited { // user registered new log associated with this test
                        
                        let log = Log(test: test.id, unit: (test.activeUnit?.id)!, value: test.value!, datetime: self.date.timeIntervalSince1970)
                        
                        do {
                            try log.insert()
                            
                            // update test_unit relations regarding newly selected unit
                            for unit in test.units {
                                let testUnit = TestUnit(testId: test.id, unitId: unit.id)
                                try testUnit.query()
                                if unit == test.activeUnit {
                                    testUnit.activate = true
                                } else {
                                    testUnit.activate = false
                                }
                                try testUnit.update()
                            }
                        } catch {
                            
                        }
                    }
                }
            }
        }.then { () -> Void in
            self.dismiss(animated: true, completion: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.Notificatoin.didUpdateData), object: nil)
        }.catch { error in
            let alert = UIAlertController(title: "Error", message: "Couldn't save your logs!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                
            }))
            self.present(alert, animated: true, completion: nil)
        }.always {
            SVProgressHUD.dismiss()
        }
        
    }
    
    /**
     action when user set date for new test log
     
     - author: harleyguru
     
     - parameter _: sender
     */
    @IBAction func onDate(_: AnyObject) {
        
        let datePickerVC = HSDatePickerViewController()
        datePickerVC.delegate = self
        self.navigationController?.present(datePickerVC, animated: true, completion: nil)
    }
    
}

// MARK: - HSDatePickerViewController Delegate

extension TestVC: HSDatePickerViewControllerDelegate {
    
    func hsDatePickerPickedDate(_ date: Date!) {
        self.dateButton.isEnabled = true
        
        self.date = date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, yyyy, h:mm a"
        let dateString = dateFormatter.string(from: self.date as Date)
        self.dateButton.setTitle(dateString, for: UIControlState.normal)
    }
    
}

// MARK: - UITableViewDelegate
extension TestVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: TestCell.identifier, for: indexPath as IndexPath) as! TestCell
        
        var test: Test? = nil
        
        if self.searchController.isActive && self.searchController.searchBar.text != "" { // user is searching
            
            if let filteredTests = self.filteredTests {
                let key = filteredTests.keys.sorted(by: <)[indexPath.section]
                test = filteredTests[key]?[indexPath.row]
            }
        } else { // normal case
            
            if let indexedTests = self.indexedTests {
                let key = indexedTests.keys.sorted(by: <)[indexPath.section]
                test = indexedTests[key]?[indexPath.row]
            }
        }
        
        if let test = test {
            cell.test = test
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let test: Test!
        
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        
        if self.searchController.isActive && self.searchController.searchBar.text != "" { // search case
            
            let key = (self.filteredTests?.keys.sorted(by: <)[indexPath.section])!
            test = (self.filteredTests![key])![indexPath.row]
            
        } else { // normal case
            let key = (self.indexedTests?.keys.sorted(by: <)[indexPath.section])!
            test = (self.indexedTests![key])![indexPath.row]
        }
        
        self.performSegue(withIdentifier: "TestResultVC", sender: test)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.searchController.isActive && self.searchController.searchBar.text != "" {
            
            if let filteredTests = self.filteredTests {
                
                let key = filteredTests.keys.sorted(by: <)[section]
                return (filteredTests[key]?.count)!
            }
            
            return 0
        }
        
        if let indexedTests = self.indexedTests {
            let key = indexedTests.keys.sorted(by: <)[section]
            return (indexedTests[key]?.count)!
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if self.searchController.isActive && self.searchController.searchBar.text != "" { // search status
            
            if let test = self.filteredTests![self.filteredTests!.keys.sorted(by: <)[indexPath.section]]?[indexPath.row] {
                if test.edited {
                    return true
                }
            }
            
        } else { // normal status
            if let test = self.indexedTests![self.indexedTests!.keys.sorted(by: <)[indexPath.section]]?[indexPath.row] {
                if test.edited {
                    return true
                }
            }
        }
        
        return false
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .default, title: "Delete") { (action, indexPath) in
            if self.searchController.isActive && self.searchController.searchBar.text != "" { // search status
                
                if let test = self.filteredTests![self.filteredTests!.keys.sorted(by: <)[indexPath.section]]?[indexPath.row] {
                    test.edited = false
                    test.value = nil
                }
                
            } else { // normal status
                if let test = self.indexedTests![self.indexedTests!.keys.sorted(by: <)[indexPath.section]]?[indexPath.row] {
                    test.edited = false
                    test.value = nil
                }
            }
            
            tableView.reloadData()
        }
        
        return [deleteAction]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if self.searchController.isActive && self.searchController.searchBar.text != "" {
            if let filteredTests = self.filteredTests {
                return filteredTests.keys.count
            }
            
            return 0
        }
        
        if let tests = self.indexedTests {
            return tests.keys.count
        }
        
        return 0

    }
    
    
    /*
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableHeaderFooterViewWithIdentifier(TableHeaderView.identifier) as! TableHeaderView
        
        var key: String?
        
        if self.searchController.active && self.searchController.searchBar.text != "" { // user is on the search status
            key = self.filteredTests?.keys.sort(<)[section]
        } else { // normal status
            key = self.indexedTests?.keys.sort(<)[section]
        }
        
        cell.titleLabel.text = key
        
        return cell
    }*/
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        
        if self.searchController.isActive && self.searchController.searchBar.text != "" { // user is on the search status
            return Array((self.filteredTests?.keys.sorted(by: <))!)
        }
        
        // user is on the raw tests page
        return Array((self.indexedTests?.keys.sorted(by: <))!)
    }
    
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        
        var sections: [String]?
        
        if self.searchController.isActive && self.searchController.searchBar.text != "" { // user is on the search status
            sections = Array((self.filteredTests?.keys.sorted(by: <))!)
        } else { // user is on the raw tests page
            sections = Array((self.indexedTests?.keys.sorted(by: <))!)
        }
        
        return (sections?.index(of: title))!
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if self.searchController.isActive && self.searchController.searchBar.text != "" { // user is on the search status
            return self.filteredTests?.keys.sorted(by: <)[section]
        } else { // user is on the raw tests page
            return self.indexedTests?.keys.sorted(by: <)[section]
        }
    }
}

// MARK: - UIBarPositioningDelegate
extension TestVC: UIBarPositioningDelegate {
    func positionForBar(bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
}
