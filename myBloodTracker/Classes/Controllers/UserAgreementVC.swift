//
//  UserAgreementVC.swift
//  myBloodTracker
//
//  Created by harleyguru on 6/24/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit

/// View Controller to display User Agreement
class UserAgreementVC: UIViewController {
    
    @IBOutlet weak var agreeButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        agreeButton.layer.borderColor = UIColor.red.cgColor
        agreeButton.layer.borderWidth = 1
        agreeButton.layer.cornerRadius = 5
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func onAgree(sender: AnyObject) {
        
        self.dismiss(animated: true, completion: nil)
    }
}
