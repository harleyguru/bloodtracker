//
//  LogsVC.swift
//  myBloodTracker
//
//  Created by harleyguru on 6/24/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit
import SVProgressHUD
import PromiseKit
import MJRefresh

/// View Controller for Log Tab
class LogsVC: UIViewController {
    
    /// dispatch once token for displaying UserAgreement page once
    let token = "user_agreement"
    
    /// logs table view
    @IBOutlet weak var tableView: UITableView!
    
    /// add bar button to add new log
    var addButton: UIBarButtonItem!
    
    /// edit bar button to edit existing log
    var editButton: UIBarButtonItem!
    
    /// backbone logs
    var _logs: [Log]? = nil
    
    /// log array to list on this page
    var logs: [Log]? {
        
        get {
            if _logs != nil {
                return _logs
            }
            
            _logs = try? Log.queryAll()
            
            return _logs
        }
    }
    
    
    /// backbone property of indexedLogs
    var _indexedLogs: [String: [Log]]? = nil
    
    /// logs converted into year-month key dictionary for indexing
    var indexedLogs: [String: [Log]]! {
        
        get {
            
            if _indexedLogs == nil {
                
                SVProgressHUD.show()

                _ = DispatchQueue.global().promise(execute: {
                    self._indexedLogs = [String: [Log]]()
                    
                    if let logs = self.logs {
                        for log in logs {
                            let key = Date.yearMonthString(from: log.datetime)
                            if !self._indexedLogs!.keys.contains(key) { // this is new section to add
                                
                                var matches = self.logs?.filter({ (log) -> Bool in
                                    if Date.yearMonthString(from: log.datetime) == key {
                                        return true
                                    }
                                    
                                    return false
                                })
                                
                                // sort matches by datetime
                                matches = matches?.sorted(by: { (pre, next) -> Bool in
                                    
                                    if pre.datetime > next.datetime {
                                        return true
                                    } else {
                                        return false
                                    }
                                    
                                })
                                
                                self._indexedLogs![key] = matches
                            }
                        }
                    }
                }).then(execute: { () -> Void in
                    // processing sortedKeys based on the current indexedLogs
                    _ = self.sortedKeys
                }).then(execute: { () -> Void in
                    self.tableView.reloadData()
                }).always {
                    self.tableView.mj_header.endRefreshing()
                    SVProgressHUD.dismiss()
                }
                
            }
            
            return _indexedLogs
        }
    }
    
    /// backbone of sortedKeys property
    var _sortedKeys: [String]!
    
    /// sorted array of keys in indexedLogs
    var sortedKeys: [String]! {
        get {
            if _sortedKeys == nil {
                _sortedKeys = self.indexedLogs!.keys.sorted(by: { (pre, next) -> Bool in
                    
                    let preDate = Date.date(from: pre)
                    let nextDate = Date.date(from: next)
                    
                    let result = preDate.compare(nextDate as Date)
                    
                    switch result {
                    case .orderedDescending:
                        return true
                        
                    case .orderedAscending:
                        return false
                        
                    case .orderedSame:
                        return true
                    }
                    
                })
            }
            
            return _sortedKeys
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.tableView.mj_header = MJRefreshNormalHeader.init(refreshingBlock: { [weak self] in
            self?.reloadLogs()
        })
        
        // register as an observer for update database notification
        NotificationCenter.default.addObserver(self, selector: #selector(LogsVC.reloadLogs), name: NSNotification.Name(rawValue: Constants.Notificatoin.didUpdateData), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // setup navigation item
        self.tabBarController?.title = "Logs"
        
        self.addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.onAdd(_:)))
        
        self.editButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(self.onEdit(_:)))
        
        self.tabBarController?.navigationItem.rightBarButtonItem = self.editButton
        self.tabBarController?.navigationItem.leftBarButtonItem = self.addButton
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        DispatchQueue.once(token: token) {
            let userAgreementVC = self.storyboard?.instantiateViewController(withIdentifier: "UserAgreement")
            AppDelegate.appDelegate.navigationController.present(userAgreementVC!, animated: false, completion: nil)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Action
    
    @IBAction func onSaveLog(_ segue: UIStoryboardSegue) {
        
    }
    
    @IBAction func onCancelLog(_ segue: UIStoryboardSegue) {
        
    }
    
    /**
     user taps on cancel button in TestVC
     
     - author: harleyguru
     
     - parameter segue: unwind segue
     */
    @IBAction func onCancelTest(_ segue: UIStoryboardSegue) {
        
    }
    
    /**
     called when user click on add button of navigation bar
     
     - author: harleyguru
     
     - parameter _: add bar button item
     */
    func onAdd(_: AnyObject) {
        self.performSegue(withIdentifier: "TestVC", sender: nil)
    }
    
    /**
     called when user clicks on edit button to edit logs from navigation bar
     
     - author: harleyguru
     
     - parameter _: edit bar button item
     */
    func onEdit(_: AnyObject) {
        
        self.isEditing = !self.isEditing
        
    }
    
    func onAgree(notification: NSNotification) {
        
        self.tabBarController?.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        
        super.setEditing(editing, animated: animated)
        
        self.tableView.isEditing = !self.tableView.isEditing
        
        
        if editing {
            self.editButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.onEdit(_:)))
        } else {
            self.editButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(self.onEdit(_:)))
        }
        self.tabBarController?.navigationItem.rightBarButtonItem = self.editButton
        
        self.tableView.reloadData()
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "EnterLabResult" {
            let testResultVC = segue.destination as! TestResultVC
            testResultVC.log = sender as! Log
        }
    }
    
    // MARK: - Helper Methods
    
    /**
     get log at specific indexPath in tableview
     
     - author: harleyguru
     
     - parameter indexPath: indexPath in tableview
     
     - returns: log at specific indexpath in tableview
     */
    func log(at indexPath: IndexPath) -> Log {
        
        let key = self.sortedKeys[indexPath.section]
        let log = self.indexedLogs[key]![indexPath.row]
        
        return log
    }
    
    
    /// load logs and indexedLogs again
    func reloadLogs() {
        
        self._logs = nil
        self._indexedLogs = nil
        self._sortedKeys = nil
        
        self.tableView.reloadData()
    }

}

// MARK: - UITableViewDelegate

extension LogsVC: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: LogCell = tableView.dequeueReusableCell(withIdentifier: LogCell.identifier, for: indexPath as IndexPath) as! LogCell
        
        cell.log = self.log(at: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard self.indexedLogs != nil else {
            return 0
        }
        
        if self.indexedLogs.count > 0 {
            
            let key = self.sortedKeys[section]
            return (self.indexedLogs[key])!.count
        }
        
        return 0
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let indexedLogs = self.indexedLogs {
            if indexedLogs.count > 0 {
                return indexedLogs.keys.count
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        
        let log = self.log(at: indexPath)
        
        self.performSegue(withIdentifier: "EnterLabResult", sender: log)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        guard self.indexedLogs != nil else {
            return nil
        }
        
        if self.indexedLogs.count > 0 {
            return self.sortedKeys[section]
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .default, title: "Delete") { (action, indexPath) in
            
            let log = self.log(at: indexPath)
            
//            let numberOfRows = self.tableView(tableView, numberOfRowsInSection: indexPath.section)
            
            do {
                
                // delete log in database
                try log.delete()
                
                // refresh UI with animation
//                tableView.beginUpdates()
//                
//                // refresh model
//                self._logs = nil
//                self._indexedLogs = nil
//                self._sortedKeys = nil
//                
//                if numberOfRows > 1 {
//                    tableView.deleteRows(at: [indexPath], with: .automatic)
//                } else {
//                    tableView.deleteSections([indexPath.section], with: .automatic)
//                }
//                
//                tableView.endUpdates()
                
                self.reloadLogs()
                
            } catch {
                
            }
            
        }
        
        return [deleteAction]
    }
    
}
