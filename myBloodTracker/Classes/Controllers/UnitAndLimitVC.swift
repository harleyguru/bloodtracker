//
//  UnitAndLimitVC.swift
//  myBloodTracker
//
//  Created by harleyguru on 10/19/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit

/// Units and Limits settings view controller
class UnitAndLimitVC: TestInfoBaseVC {
    
    /// constants for section numbers in table view
    let SectionName = 0
    let SectionUnits = 1
    let SectionLimits = 2
    let SectionInfo = 3
    let SectionAbbreviation = 4
    let SectionGroup = 5
    
    /// bottom layout contraint from table view to bottom layout guide
    @IBOutlet weak var bottomTableviewLayoutContraint: NSLayoutConstraint!
    weak var settingsViewController: TestSettingsVC!
    
    /// minimum value text field
    var minTextField: UITextField!
    
    /// maximum value text field
    var maxTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // load full information for this test including units and groups associated with it
        do {
            try self.test.queryUnit()
            try self.test.queryGroup()
        } catch {
            let alert = UIAlertController(title: "Error", message: "Sorry, couldn't load data!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                _ = self.navigationController?.popViewController(animated: true)
            }))
            present(alert, animated: true, completion: nil)
            return
        }
        
        self.oldActiveUnitIndex = self.test.activeUnitIndex
        
        NotificationCenter.default.addObserver(self, selector: #selector(TestVC.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TestVC.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: Observation
    
    /**
     called when the keyboard will be hidden
     
     - author: harleyguru
     
     - parameter notification: notification got from notification center
     */
    func keyboardWillHide(notification: NSNotification) {
        let rate = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        
        UIView.animate(withDuration: rate.doubleValue) {
            self.bottomTableviewLayoutContraint.constant = 0
        }
    }
    
    /**
     called when the keyboard will be shown
     
     - author: harleyguru
     
     - parameter notification: notification got from notification center
     */
    func keyboardWillShow(notification: NSNotification) {
        let keyboardSize = (notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue.size
        let rate = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        
        UIView.animate(withDuration: rate.doubleValue) {
            self.bottomTableviewLayoutContraint.constant = keyboardSize.height
        }
        
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "goBackSettings" {
            if sender == nil { // user clicked on cancel button
                self.settingsViewController.isUpdated = false
            } else { // user clicked on done button
                // settings view controller needs to update contents with updated information
                self.settingsViewController.isUpdated = true
            }
        }
    }
    
    // MARK: - Action
    
    /// user clicked on done button of navigation bar
    ///
    /// - parameter _: done button
    @IBAction func onDone(_: AnyObject) {
        self.maxTextField.resignFirstResponder()
        self.minTextField.resignFirstResponder()
        
        // save changes for the test
        do {
            // update changes for this test
            try self.test.update()
            
            // update test_unit relations regarding newly selected unit
            for unit in self.test.units {
                let testUnit = TestUnit(testId: self.test.id, unitId: unit.id)
                try testUnit.query()
                if unit == self.test.activeUnit {
                    testUnit.activate = true
                } else {
                    testUnit.activate = false
                }
                try testUnit.update()
            }
        } catch {
            
        }
        
        self.performSegue(withIdentifier: "goBackSettings", sender: self.doneButton)
    }
    
    /// user clicked on navigation bar cancel button
    ///
    /// - parameter _: cancel button
    @IBAction func onCancel(_: AnyObject) {
        self.maxTextField.resignFirstResponder()
        self.minTextField.resignFirstResponder()
        
        // roll back test to origin information
        do {
            try self.test.query()
            try self.test.queryUnit()
        } catch {
            
        }
        
        self.performSegue(withIdentifier: "goBackSettings", sender: nil)
    }
}

// MARK: - UITableViewDelegate

extension UnitAndLimitVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier: String!
        
        // setting reuse identifier
        if indexPath.section == SectionUnits { // units section
            
            cellIdentifier = "LabCell1"
            
        } else if indexPath.section == SectionInfo  || indexPath.section == SectionName { // information section and name section
            
            cellIdentifier = InfoCell.identifier
            
        } else if indexPath.section == SectionLimits { // limits section
            
            cellIdentifier = LimitCell.identifier
        } else {
            cellIdentifier = "LabCell2"
        }
        
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        
        if cell == nil { // create cell
            cell = UITableViewCell(style: .value1, reuseIdentifier: cellIdentifier)
        }
        
        // configure cell contents
        if indexPath.section == SectionUnits { // units section
            
            let unit = self.test.units[indexPath.row]
            
            cell?.textLabel?.text = unit.name
            cell?.textLabel?.textColor = UIColor.black
            cell?.detailTextLabel?.text = nil
            
            if unit.active {
                cell?.accessoryType = .checkmark
            } else {
                cell?.accessoryType = .none
            }
            
        } else if indexPath.section == SectionLimits { // limits section
            
            let limitCell = cell as! LimitCell
            limitCell.selectionStyle = .none
            limitCell.row = indexPath.row
            limitCell.limitViewController = self
            limitCell.test = self.test
            if indexPath.row == 0 { // this is maximum cell
                self.maxTextField = limitCell.limitField
            } else { // this is minimum cell
                self.minTextField = limitCell.limitField
            }
            
            return limitCell
            
        } else if indexPath.section == SectionName { // name section
            
            let nameCell = cell as! InfoCell
            
            nameCell.infoTextView.text = test.name
            
            nameCell.infoTextView.isScrollEnabled = false
            nameCell.infoTextView.isEditable = false
            nameCell.selectionStyle = .none
            
            return nameCell
            
        } else if indexPath.section == SectionInfo { // information section
            
            let infoCell = cell as! InfoCell
            
            if let info = test.info {
                infoCell.infoTextView.text = info
                infoCell.infoTextView.textColor = UIColor.black
            } else {
                infoCell.infoTextView.text = "No Information for this test"
                infoCell.infoTextView.textColor = UIColor.lightGray
            }
            
            infoCell.infoTextView.isScrollEnabled = false
            infoCell.infoTextView.isEditable = false
            infoCell.selectionStyle = .none
            
            return infoCell
        } else if indexPath.section == SectionAbbreviation { // abbreviation section
            
            if let abbreviation = test.abbreviation {
                cell?.textLabel?.text = abbreviation
                cell?.textLabel?.textColor = UIColor.black
            } else {
                cell?.textLabel?.text = "No abbreviation for this test"
                cell?.textLabel?.textColor = UIColor.lightGray
            }
            
            cell?.detailTextLabel?.text = nil
            cell?.accessoryType = .none
            cell?.selectionStyle = .none
            
        } else if indexPath.section == SectionGroup { // group section
            
            let group = self.test.groups[0]
            
            if let name = group.name {
                cell?.textLabel?.text = name
                cell?.textLabel?.textColor = UIColor.black
            } else {
                cell?.textLabel?.text = "This test doesn't belong to any group"
                cell?.textLabel?.textColor = UIColor.lightGray
            }
            
            cell?.detailTextLabel?.text = nil
            
            cell?.accessoryType = .none
            cell?.selectionStyle = .none
            
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        
        if indexPath.section == SectionUnits { // user changes unit
            
            self.test.activeUnitIndex = indexPath.row
            
            let delayTime = DispatchTime.now() + 0.2
            
            DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
                tableView.reloadData()
            })
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == SectionName {
            return 1
        } else if section == SectionUnits { // units section
            return self.test.units.count
        } else if section == SectionLimits { // limits section
            return 2
        } else if section == SectionInfo { // information section
            return 1
        } else if section == SectionAbbreviation { // abbreviation section
            return 1
        } else if section == SectionGroup { // group section
            //            return self.test.groups.count
            return 1
        }
        
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section == SectionName {
            return "Name"
        } else if section == SectionUnits { // units section
            return "Units"
        } else if section == SectionLimits { // limits section
            return "Reference limits"
        } else if section == SectionInfo { // information section
            return "Information"
        } else if section == SectionAbbreviation { // abbreviation section
            return "Abbreviation"
        } else if section == SectionGroup { // group section
            return "Group"
        }
        
        return nil
    }
    
}

