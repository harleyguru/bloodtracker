
//
//  GraphPageVC.swift
//  myBloodTracker
//
//  Created by harleyguru on 9/23/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit

/// Graph page view controller to be displayed for each page
class GraphPageVC: UIViewController {
    
    /// page index of this page
    var index: Int!
    
    /// all tests
    var tests: [Test]!
    
    /// parent graph view controller
    weak var graphVC: GraphVC!
    
    /// tests to be displayed in this page
    var testsForPage: [Test]! {
        get {
            let startIndex = self.index * self.graphVC.countPerPage
            let endIndex = startIndex + (self.graphVC.countPerPage - 1)

            var tests = [Test]()
            
            if self.tests == nil || self.tests.count == 0 {
                return tests
            }
            
            if endIndex < self.tests.count - 1 {
                tests = Array(self.tests[startIndex...endIndex])
            } else {
                tests = Array(self.tests[startIndex...(self.tests.count - 1)])
            }
            
            return tests
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        let graphInfoVC = segue.destination as! GraphInfoVC
        let cell: GraphCell! = sender as! GraphCell
        graphInfoVC.test = cell.test
    }
    
    // MARK: - Action
    
    /// action called when the user taps on close button in graph info page
    ///
    /// - parameter segue: unwind segue
    @IBAction func onCloseDetail(_ segue: UIStoryboardSegue) {
        
    }
    
}

extension GraphPageVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: GraphCell.identifier, for: indexPath as IndexPath) as! GraphCell
        
        let test = self.testsForPage[indexPath.row]
        
        cell.test = test
        cell.graphViewController = self
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.testsForPage.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}


