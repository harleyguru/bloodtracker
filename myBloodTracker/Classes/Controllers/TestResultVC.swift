//
//  TestResultVC.swift
//  myBloodTracker
//
//  Created by harleyguru on 8/3/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit

/// Represents Enter Lab Result page
class TestResultVC: TestInfoBaseVC {
    
    /// constants for section numbers in table view
    let SectionValue = 0
    let SectionUnits = 1
    let SectionLimits = 2
    let SectionInfo = 3
    let SectionAbbreviation = 4
    let SectionGroup = 5
    
    
    /// old test value come up from test view controller
    var oldValue: Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // load full information for this test including units and groups associated with it
        
        if !self.test.edited { // if user comes for registering new test log only
            
            do {
                try self.test.queryUnit()
                try self.test.queryGroup()
            } catch {
                let alert = UIAlertController(title: "Error", message: "Sorry, couldn't load data!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                    _ = self.navigationController?.popViewController(animated: true)
                }))
                present(alert, animated: true, completion: nil)
                return
            }
        }
        
        // load old test value, current updated value will be stored in test itself
        self.oldValue = self.test.value
        self.oldActiveUnitIndex = self.test.activeUnitIndex
        
        if self.test.value != nil {
            self.doneButton.isEnabled = true
        } else {
            self.doneButton.isEnabled = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }

    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "onSaveResult" {
            
        } else if segue.identifier == "onCancelResult" {
            
        }
    }
    
    
    // MARK: - Action
    
    /**
     Its called actoin when user taps on done button in navigation bar
     
     - author: harleyguru
     
     - parameter _: sender
     */
    @IBAction func onSaveResult(_: AnyObject) {
        
        if self._log != nil { // send back to logs page by saving the result
            if self.test.value != nil {
                self.log.value = self.test.value
                self.log.testId = self.test.id
                self.log.unitId = self.test.activeUnit?.id
                
                do {
                    try self.log.update()
                    
                    // update test_unit relations regarding newly selected unit
                    for unit in self.test.units {
                        let testUnit = TestUnit(testId: self.test.id, unitId: unit.id)
                        try testUnit.query()
                        if unit == self.test.activeUnit {
                            testUnit.activate = true
                        } else {
                            testUnit.activate = false
                        }
                        try testUnit.update()
                    }
                } catch {
                    let alert = UIAlertController(title: "Error", message: "Sorry, couldn't update log!", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                        _ = self.navigationController?.popViewController(animated: true)
                    }))
                    present(alert, animated: true, completion: nil)
                    return
                }
                
                self.performSegue(withIdentifier: "onSaveLog", sender: nil)
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.Notificatoin.didUpdateData), object: nil)
            }
        } else { // send back to tests page by saving the result
            if self.test.value != nil {
                self.test.edited = true
                self.performSegue(withIdentifier: "onSaveResult", sender: nil)
            }
        }
        
    }
    
    /**
     Its called exit action when user taps on cancel button in navigation bar
     
     - author: harleyguru
     
     - parameter _: sender
     */
    @IBAction func onCancelResult(_: AnyObject) {
        
        if self._log != nil { // send back to logs page without saving result
            self.performSegue(withIdentifier: "onCancelLog", sender: nil)
        } else { // send back to tests page without saving result
            self.test.value = self.oldValue
            self.test.activeUnitIndex = self.oldActiveUnitIndex
            self.performSegue(withIdentifier: "onCancelResult", sender: nil)
        }
        
    }
    
    /**
     action when user taps on done button for inputing value in toolbar
     
     - author: harleyguru
     
     - parameter _: sender
     */
    @IBAction func onDone(_: AnyObject) {
        
        self.test.value = Double(self.textField.text!)
        
        self.textField.resignFirstResponder()
    }
    
    /**
     action when user taps on cancel button for inputing value in toolbar
     
     - author: harleyguru
     
     - parameter _: sender
     */
    @IBAction func onCancel(_: AnyObject) {
        
        self.test.value = nil
        
        self.tableView.reloadData()
        
        self.textField.text = nil
        
        self.textField.resignFirstResponder()
    }

}

// MARK: - UITextFieldDelegate

extension TestResultVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.textField.resignFirstResponder()
    
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text = self.textField.text! as NSString
        let newText = text.replacingCharacters(in: range, with: string)
        
        self.test.value = Double(newText)
        
        if self.test.value != nil {
            self.doneButton.isEnabled = true
        } else {
            self.doneButton.isEnabled = false
            self.textField.text = ""
        }
        
        self.tableView.reloadData()
        
        return true
    }
}

// MARK: - UITableViewDelegate

extension TestResultVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier: String!
        
        // setting reuse identifier
        if indexPath.section == SectionUnits { // units section
            
            cellIdentifier = "LabCell1"
            
        } else if indexPath.section == SectionInfo { // information section
            
            cellIdentifier = InfoCell.identifier
            
        } else { // value and limits section
            
            cellIdentifier = "LabCell2"
        }
        
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        
        if cell == nil { // create cell
            cell = UITableViewCell(style: .value1, reuseIdentifier: cellIdentifier)
        }
        
        // configure cell contents
        if indexPath.section == SectionValue { // value section
            
            cell?.textLabel?.text = "Value"
            cell?.textLabel?.textColor = UIColor.black
            
            if let lastValue = self.test.value {
                
                var attribute = [NSForegroundColorAttributeName: UIColor(red:0.26, green:0.61, blue:0.26, alpha:1.0)]
                
                if lastValue < self.test.min! || lastValue > self.test.max! {
                    attribute = [NSForegroundColorAttributeName: UIColor.red]
                }
                
                let attributedValueString = NSMutableAttributedString(string: "\(lastValue)", attributes: attribute)
                attributedValueString.append(NSAttributedString(string: " " + self.test.activeUnit!.name!))
                
                cell?.detailTextLabel?.attributedText = attributedValueString
                
            } else {
                cell?.detailTextLabel?.text = "No Value"
            }
            
            cell?.accessoryType = .disclosureIndicator
            
        } else if indexPath.section == SectionUnits { // units section
            
            let unit = self.test.units[indexPath.row]
            
            cell?.textLabel?.text = unit.name
            cell?.textLabel?.textColor = UIColor.black
            cell?.detailTextLabel?.text = nil
            
            if unit.active {
                cell?.accessoryType = .checkmark
            } else {
                cell?.accessoryType = .none
            }
            
        } else if indexPath.section == SectionLimits { // limits section
            
            cell?.selectionStyle = .none
            cell?.textLabel?.textColor = UIColor.black
            
            if indexPath.row == 0 { // maximum
                
                cell?.textLabel?.text = "Maximum"
                cell?.detailTextLabel?.text = "\(self.test.max!) \(self.test.activeUnit!.name!)"
                
            } else { // minimum
                
                cell?.textLabel?.text = "Minimum"
                cell?.detailTextLabel?.text = "\(self.test.min!) \(self.test.activeUnit!.name!)"
                
            }
            
        } else if indexPath.section == SectionInfo { // information section
            
            let infoCell = cell as! InfoCell
            
            if let info = test.info {
                infoCell.infoTextView.text = info
                infoCell.infoTextView.textColor = UIColor.black
            } else {
                infoCell.infoTextView.text = "No Information for this test"
                infoCell.infoTextView.textColor = UIColor.lightGray
            }
            
            infoCell.infoTextView.isScrollEnabled = false
            infoCell.infoTextView.isEditable = false
            infoCell.selectionStyle = .none
            
            return infoCell
        } else if indexPath.section == SectionAbbreviation { // abbreviation section
            
            if let abbreviation = test.abbreviation {
                cell?.textLabel?.text = abbreviation
                cell?.textLabel?.textColor = UIColor.black
            } else {
                cell?.textLabel?.text = "No abbreviation for this test"
                cell?.textLabel?.textColor = UIColor.lightGray
            }
            
            cell?.detailTextLabel?.text = nil
            cell?.accessoryType = .none
            cell?.selectionStyle = .none
            
        } else if indexPath.section == SectionGroup { // group section
            
            let group = self.test.groups[0]
            
            if let name = group.name {
                cell?.textLabel?.text = name
                cell?.textLabel?.textColor = UIColor.black
            } else {
                cell?.textLabel?.text = "This test doesn't belong to any group"
                cell?.textLabel?.textColor = UIColor.lightGray
            }
            
            cell?.detailTextLabel?.text = nil
            
            cell?.accessoryType = .none
            cell?.selectionStyle = .none
            
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == SectionValue { // user taps on value button
            
            if let lastValue = self.test.value {
                self.textField.text = "\(lastValue)"
            }
            
            self.textField.becomeFirstResponder()
            
        } else if indexPath.section == SectionUnits { // user changes unit
            
            self.test.activeUnitIndex = indexPath.row
            
            let delayTime = DispatchTime.now() + 0.2
            
            DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
                tableView.reloadData()
            })
            
        }
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == SectionValue { // value section
            return 1
        } else if section == SectionUnits { // units section
            return self.test.units.count
        } else if section == SectionLimits { // limits section
            return 2
        } else if section == SectionInfo { // information section
            return 1
        } else if section == SectionAbbreviation { // abbreviation section
            return 1
        } else if section == SectionGroup { // group section
//            return self.test.groups.count
            return 1
        }
        
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section == SectionValue { // value section
            return self.test.name
        } else if section == SectionUnits { // units section
            return "Units"
        } else if section == SectionLimits { // limits section
            return "Reference limits"
        } else if section == SectionInfo { // information section
            return "Information"
        } else if section == SectionAbbreviation { // abbreviation section
            return "Abbreviation"
        } else if section == SectionGroup { // group section
            return "Group"
        }
        
        return nil
    }
    
}

