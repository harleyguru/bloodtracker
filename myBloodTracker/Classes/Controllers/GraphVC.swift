//
//  GraphVC.swift
//  myBloodTracker
//
//  Created by harleyguru on 6/24/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit
import SwiftCharts

/// Graph view controller
class GraphVC: UIPageViewController {
    
    /// graph count to show per a page
    let countPerPage = 10
    
    /// backbone tests with just valid logs
    var _tests: [Test]?
    
    /// all tests to show across all pages with just valid logs only
    var tests: [Test]? {
        get {
            if _tests == nil {
                do {
                    let allTests = try Test.queryAll()
                    let tests = allTests.filter({ (test) -> Bool in
                        let logs = try? test.queryLogs()
                        if logs!.count > 0 {
                            return true
                        }
                        return false
                    })
                    
                    _tests = tests
                } catch {
                    
                }
                
            }
            
            return _tests
        }
    }
    
    /// total page count
    var totalPage: Int! {
        get {
            if let tests = self.tests {
                return (tests.count % self.countPerPage) == 0 ? (tests.count / self.countPerPage) : (tests.count / self.countPerPage + 1)
            }
            
            return 0
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.dataSource = self
        self.delegate = self
        
        self.view.backgroundColor = .white
        
        let pageAppearance = UIPageControl.appearance()
        pageAppearance.pageIndicatorTintColor = .lightGray
        pageAppearance.currentPageIndicatorTintColor = .red
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // setup navigation item
        self.tabBarController?.title = "Graph"
        self.tabBarController?.navigationItem.leftBarButtonItem = nil
        self.tabBarController?.navigationItem.rightBarButtonItem = nil
        
        self._tests = nil
        if let _ = self.tests {
            
            self.setViewControllers([getGraphPage(index: 0)], direction: .forward, animated: true, completion: { [unowned self] (isFinished) in
                if isFinished {
                    DispatchQueue.main.async {
                        self.setViewControllers([self.getGraphPage(index: 0)], direction: .forward, animated: false, completion: nil)
                    }
                }
            })
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Helper Methods
    
    
    /// returns graph page view controller to display for each page
    ///
    /// - parameter index: page index
    ///
    /// - returns: graph page view controller to display for each page
    func getGraphPage(index: Int) -> GraphPageVC {
        
        let graphPage = storyboard?.instantiateViewController(withIdentifier: "GraphPageVC") as! GraphPageVC
        graphPage.index = index
        graphPage.tests = self.tests
        graphPage.graphVC = self
        
        return graphPage
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}

extension GraphVC: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        let pageVC = viewController as! GraphPageVC
        
        if pageVC.index <= 0 {
            return nil
        }

        return getGraphPage(index: pageVC.index - 1)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        let pageVC = viewController as! GraphPageVC
        
        if pageVC.index >= self.totalPage - 1 {
            return nil
        }
        
        return getGraphPage(index: pageVC.index + 1)
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        
        return self.totalPage
        
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
}

