//
//  GraphInfoVC.swift
//  myBloodTracker
//
//  Created by harleyguru on 10/16/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit
import SwiftCharts

/// Detail View Controller for displaying Graph Information
class GraphInfoVC: UIViewController {
    
    /// constants for section numbers in table view
    let SectionValue = 0
    let SectionUnits = 1
    let SectionLimits = 2
    let SectionInfo = 3
    let SectionAbbreviation = 4
    let SectionGroup = 5
    
    /// navigation bar
    @IBOutlet weak var navigationBar: UINavigationBar!
    
    /// custom navigation item
    @IBOutlet weak var navigationItem1: UINavigationItem!
    
    /// table view to display detail of test
    @IBOutlet weak var tableView: UITableView!
    
    /// test user associated with this graph view
    var test: Test!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationItem1.title = self.test?.name
        
        // configure table view
        self.tableView.estimatedRowHeight = 44
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        // load full information for this test including units and groups associated with it
        
        do {
            try self.test.queryUnit()
            try self.test.queryGroup()
            self.tableView.reloadData()
        } catch {
            let alert = UIAlertController(title: "Error", message: "Sorry, couldn't load data!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                _ = self.navigationController?.popViewController(animated: true)
            }))
            present(alert, animated: true, completion: nil)
            
            return
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "showDuration" {
            let durationVC = segue.destination as! DurationVC
            durationVC.test = self.test
        }
        
    }

    // MARK: - Action
    
    /// user closed duration setting view
    ///
    /// - parameter segue: unwind segue
    @IBAction func onCloseDuration(_ segue: UIStoryboardSegue) {
        
    }
}

extension GraphInfoVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier: String!
        
        // setting reuse identifier
        if indexPath.section == SectionValue {
            cellIdentifier = GraphCell.identifier
        } else if indexPath.section == SectionUnits { // units section
            
            cellIdentifier = "LabCell1"
            
        } else if indexPath.section == SectionInfo { // information section
            
            cellIdentifier = InfoCell.identifier
            
        } else { // value and limits section
            
            cellIdentifier = "LabCell2"
        }
        
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        
        if cell == nil { // create cell
            cell = UITableViewCell(style: .value1, reuseIdentifier: cellIdentifier)
        }
        
        // configure cell contents
        if indexPath.section == SectionValue { // value section
            
            let graphCell = cell as! GraphCell
            graphCell.test = self.test
            graphCell.graphViewController = nil
            
        } else if indexPath.section == SectionUnits { // units section
            
            print(">>>>> unit count: \(self.test.units.count)")
            let unit = self.test.units[indexPath.row]
            
            cell?.textLabel?.text = unit.name
            cell?.textLabel?.textColor = UIColor.black
            cell?.detailTextLabel?.text = nil
            
            if unit.active {
                cell?.accessoryType = .checkmark
            } else {
                cell?.accessoryType = .none
            }
            
        } else if indexPath.section == SectionLimits { // limits section
            
            cell?.selectionStyle = .none
            cell?.textLabel?.textColor = UIColor.black
            
            if indexPath.row == 0 { // maximum
                
                cell?.textLabel?.text = "Maximum"
                cell?.detailTextLabel?.text = "\(self.test.max!) \(self.test.activeUnit!.name!)"
                
            } else { // minimum
                
                cell?.textLabel?.text = "Minimum"
                cell?.detailTextLabel?.text = "\(self.test.min!) \(self.test.activeUnit!.name!)"
                
            }
            
        } else if indexPath.section == SectionInfo { // information section
            
            let infoCell = cell as! InfoCell
            
            if let info = test.info {
                infoCell.infoTextView.text = info
                infoCell.infoTextView.textColor = UIColor.black
            } else {
                infoCell.infoTextView.text = "No Information for this test"
                infoCell.infoTextView.textColor = UIColor.lightGray
            }
            
            infoCell.infoTextView.isScrollEnabled = false
            infoCell.infoTextView.isEditable = false
            infoCell.selectionStyle = .none
            
            return infoCell
        } else if indexPath.section == SectionAbbreviation { // abbreviation section
            
            if let abbreviation = test.abbreviation {
                cell?.textLabel?.text = abbreviation
                cell?.textLabel?.textColor = UIColor.black
            } else {
                cell?.textLabel?.text = "No abbreviation for this test"
                cell?.textLabel?.textColor = UIColor.lightGray
            }
            
            cell?.detailTextLabel?.text = nil
            cell?.accessoryType = .none
            cell?.selectionStyle = .none
            
        } else if indexPath.section == SectionGroup { // group section
            
            let group = self.test.groups[0]
            
            if let name = group.name {
                cell?.textLabel?.text = name
                cell?.textLabel?.textColor = UIColor.black
            } else {
                cell?.textLabel?.text = "This test doesn't belong to any group"
                cell?.textLabel?.textColor = UIColor.lightGray
            }
            
            cell?.detailTextLabel?.text = nil
            
            cell?.accessoryType = .none
            cell?.selectionStyle = .none
            
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == SectionValue { // value section
            return 1
        } else if section == SectionUnits { // units section
            return self.test.units.count
        } else if section == SectionLimits { // limits section
            return 2
        } else if section == SectionInfo { // information section
            return 1
        } else if section == SectionAbbreviation { // abbreviation section
            return 1
        } else if section == SectionGroup { // group section
            //            return self.test.groups.count
            return 1
        }
        
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section == SectionValue { // value section
            return self.test.name
        } else if section == SectionUnits { // units section
            return "Units"
        } else if section == SectionLimits { // limits section
            return "Reference limits"
        } else if section == SectionInfo { // information section
            return "Information"
        } else if section == SectionAbbreviation { // abbreviation section
            return "Abbreviation"
        } else if section == SectionGroup { // group section
            return "Group"
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == SectionValue {
            return 255
        }
        
        return UITableViewAutomaticDimension
    }
    
}

