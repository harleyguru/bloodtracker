//
//  TestInfoBaseVC.swift
//  myBloodTracker
//
//  Created by harleyguru on 10/19/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit

/// Base view controller for Test Info view controllers
class TestInfoBaseVC: UIViewController {
    
    /// result table view, here user will input lab test result
    @IBOutlet weak var tableView: UITableView!
    
    /// done button in navigation bar
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    /// hidden text field to use in order to popup keyboard when user taps on value cell
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var toolbar: UIToolbar!
    
    /// test user selected from test list in TestVC
    var test: Test!
    
    /// stored property for log, its available to access when user comes from logs page
    var _log: Log?
    
    /// log user selected from Logs table in LogsVC
    var log: Log! {
        get {
            return _log
        }
        
        set {
            _log = newValue
            
            self.test = Test(id: newValue.testId)
            do {
                try self.test.query()               // load full test info from db
                try self.test.queryUnit()           // load all units of this test
                try self.test.queryGroup()          // load all group information of this test
                self.test.value = newValue.value        // set current test value from log
                let unit = Unit(id: newValue.unitId)    // set current active unit from log
                self.test.activeUnit = unit         // set active unit from log
                
            } catch {
                #if DEBUG
                    print("***** failed to load test from log")
                #endif
            }
            
        }
    }

    /// old active unit index of test come up from test view controller
    var oldActiveUnitIndex: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // configure table view
        self.tableView.estimatedRowHeight = 44
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.textField.inputAccessoryView = self.toolbar
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.textField.resignFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "onSaveResult" {
            
        } else if segue.identifier == "onCancelResult" {
            
        }
    }
}
