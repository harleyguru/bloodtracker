//
//  SettingsVC.swift
//  myBloodTracker
//
//  Created by harleyguru on 6/24/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit

/// Settings view controller for settings tab
class SettingsVC: UIViewController {
    
    /// settings table view
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // setup navigation item
        self.tabBarController?.title = "Settings"
        self.tabBarController?.navigationItem.leftBarButtonItem = nil
        self.tabBarController?.navigationItem.rightBarButtonItem = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showDisclaimer" {
            
        }
        
    }

}

extension SettingsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "settingsCell"
        
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
        }
        
        if indexPath.section == 0 { // limit and units section
            cell?.textLabel?.text = "Limits and Units"
        } else if indexPath.section == 1 { // support section
            if indexPath.row == 0 {
                cell?.textLabel?.text = "Email feedback / errors"
            } else if indexPath.row == 1 {
                cell?.textLabel?.text = "Rate app"
            }
        } else if indexPath.section == 2 { // misc section
            if indexPath.row == 0 {
                cell?.textLabel?.text = "About this app"
            } else if indexPath.row == 1 {
                cell?.textLabel?.text = "Disclaimer"
            }
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 0 { // limit and units section
            
            self.performSegue(withIdentifier: "showUnitsAndLimits", sender: nil)
            
        } else if indexPath.section == 1 { // support section
            
            if indexPath.row == 0 { // email feedback
                let url = URL(string: "mailto:mwmazzeo@inthedojo.com")
                UIApplication.shared.openURL(url!)
            } else if indexPath.row == 1 { // rate app
                UIApplication.shared.openURL(URL(string: "itms-apps://itunes.apple.com/app/id1127638357")!)
            }
            
        } else if indexPath.section == 2 { // misc section
            
            if indexPath.row == 0 { // About this app
                UIApplication.shared.openURL(URL(string: "http://www.inthedojo.com/")!)
            } else if indexPath.row == 1 { // Disclaimer
                self.performSegue(withIdentifier: "showDisclaimer", sender: nil)
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 { // limit and units section
            return 1
        } else if section == 1 { // support section
            return 2
        } else if section == 2 { // misc section
            return 2
        }
        
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 { // section for limist and units
            return "Configure Data"
        } else if section == 1 { // support section
            return "Support"
        } else if section == 2 { // Misc section
            return "Misc"
        }
        
        return nil
    }
}
