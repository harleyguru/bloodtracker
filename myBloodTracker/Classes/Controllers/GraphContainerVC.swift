//
//  GraphContainerVC.swift
//  myBloodTracker
//
//  Created by harleyguru on 9/23/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit

/// Container view controller for the whole graph pages
class GraphContainerVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
