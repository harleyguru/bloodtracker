//
//  TestBaseVC.swift
//  myBloodTracker
//
//  Created by harleyguru on 10/19/16.
//  Copyright © 2016 myBloodTracker. All rights reserved.
//

import UIKit

/// Base view controller to work as a base view controller for listing tests and search
class TestBaseVC: UIViewController {
    
    /// tests table
    @IBOutlet weak var tableView: UITableView!
    
    /// bottom layout contraint from table view to bottom layout guide
    @IBOutlet weak var bottomTableviewLayoutContraint: NSLayoutConstraint!
    
    /// search controller to control search of tests
    var searchController: UISearchController!
    
    /// backbone of tests
    var _tests: [Test]?
    
    /// test array loaded from database
    var tests: [Test]? {
    
        get {
            if _tests == nil {
                _tests = try? Test.queryAll()
            }
            
            return _tests
        }
        
        set {
            _tests = newValue
        }
    }
    
    /// tests converted into alphabet dictionary for indexing
    var indexedTests: [String: [Test]]!
    
    /// tests filtered by search bar
    var filteredTests: [String: [Test]]? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // setting up search bar for tests table
        self.searchController = UISearchController(searchResultsController: nil)
        self.searchController.searchResultsUpdater = self
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.dimsBackgroundDuringPresentation = false
        self.searchController.definesPresentationContext = true
        self.definesPresentationContext = true
        self.tableView.tableHeaderView = self.searchController.searchBar
        
        // convert tests to alphabet indexed tests
        self.convertToDictionary()
        
        // table view setting
        self.tableView.sectionIndexColor = UIColor.red
        self.tableView.sectionIndexBackgroundColor = UIColor.clear
        
        NotificationCenter.default.addObserver(self, selector: #selector(TestVC.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TestVC.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        self.searchController.view.removeFromSuperview()
        NotificationCenter.default.removeObserver(self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: Observation
    
    /**
     called when the keyboard will be hidden
     
     - author: harleyguru
     
     - parameter notification: notification got from notification center
     */
    func keyboardWillHide(notification: NSNotification) {
        let rate = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        
        UIView.animate(withDuration: rate.doubleValue) {
            self.bottomTableviewLayoutContraint.constant = 0
        }
    }
    
    /**
     called when the keyboard will be shown
     
     - author: harleyguru
     
     - parameter notification: notification got from notification center
     */
    func keyboardWillShow(notification: NSNotification) {
        let keyboardSize = (notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue.size
        let rate = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        
        UIView.animate(withDuration: rate.doubleValue) {
            self.bottomTableviewLayoutContraint.constant = keyboardSize.height
        }
        
    }

    // MARK: - Helper Methods
    
    /**
     Convert array of tests to Dictionary with alphabeta key and tests its name prefix contains that key
     
     - author: harleyguru
     
     - parameter tests: test array to convert
     
     - returns: void
     */
    func convertToDictionary() {
        
        let alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".characters.map { (character) -> String in
            return String(character)
        }
        
        self.indexedTests = [String: [Test]]()
        
        for letter in alphabet {
            
            let matches = self.tests!.filter({ (test) -> Bool in
                if test.name.hasPrefix(letter) {
                    return true
                }
                return false
            })
            if !matches.isEmpty {
                self.indexedTests[letter] = matches
            }
        }
        
    }
    
    /**
     filter indexedTests by searchText, update filteredTests dictionary and reload table with search result
     
     - author: harleyguru
     
     - parameter searchText: text to search
     */
    func filterTestsForSearchText(searchText: String) {
        
        if let indexedTests = self.indexedTests {
            
            var result: [Test]?
            self.filteredTests = [String: [Test]]()
            
            for key in indexedTests.keys {
                result = indexedTests[key]?.filter({ (test) -> Bool in
                    
                    if test.name.lowercased().contains(searchText.lowercased()) {
                        return true
                    }
                    
                    if let abbreviation = test.abbreviation {
                        
                        if abbreviation.lowercased().contains(searchText.lowercased()) {
                            return true
                        }
                        
                    }
                    
                    
                    return false
                })
                if !(result?.isEmpty)! {
                    self.filteredTests![key] = result
                }
            }
            
            self.tableView.reloadData()
        }
    }
}

// MARK: - UISearchResultsUpdating
extension TestBaseVC: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        filterTestsForSearchText(searchText: searchController.searchBar.text!)
    }
}

